﻿using Bukhatos.Domain.SeedWork;
using Bukhatos.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;


namespace Bukhatos.Domain.Entities.DocumentAggregate
{
    public class ApprovalProcess : Entity
    {
        private List<StepState> _steps;

        public IEnumerable<StepState> Steps => _steps.AsReadOnly();

        //EF
        public ApprovalProcess()
        {
            _steps = new List<StepState>();
        }

        /// <summary>
        /// Inititalizes the approval process
        /// </summary>
        /// <param name="steps">approval steps</param>
        public ApprovalProcess(int[] stepsIds)
        {
            InitSteps(stepsIds);
        }

        /// <summary>
        /// Sets all steps of the approval process to its default value
        /// </summary>
        /// <param name="userId">who started the approval process again</param>
        public void Start()
        {
            _steps = _steps.Select(
                    step => new StepState(step.GetStepId(), null, StepStatus.Pending)
                ).ToList();
        }

        public void ApproveStep(int stepId, int approvedById)
        {
            var index = GetIndexOfStep(stepId);

            if (index < 0)
                throw new BukhatosDomainException($"The process doesn't contain any step with id = {stepId}");

            var approvedStepState = new StepState(stepId, approvedById, StepStatus.Approved);
            _steps[index] = approvedStepState;
        }

        public void RejectStep(int stepId, int rejectedById, string comment, string attachedFile)
        {
            var _comment = new Comment(comment, attachedFile);

            var index = GetIndexOfStep(stepId);

            if (index < 0)
                throw new BukhatosDomainException($"The process doesn't contain any step with id = {stepId}");

            var rejectedStepState = new StepState(stepId, rejectedById, StepStatus.Rejected, _comment);
            _steps[index] = rejectedStepState;
        }

        public bool IsApproved()
        {
            bool isApproved = false;

            foreach (var step in _steps)
            {
                if (step.GetStatus() != StepStatus.Approved)
                {
                    isApproved = false;
                    break;
                }

                isApproved = true;
            }

            return isApproved;
        }

        protected void InitSteps(int[] stepsIds)
        {
            if (stepsIds == null)
                throw new ArgumentNullException(nameof(stepsIds));

            _steps = new List<StepState>();

            for(int i = 0; i < stepsIds.Length; i++)
            {
                var stepState = new StepState(stepsIds[i], null, StepStatus.Initialized);
                _steps.Add(stepState);
            }
        }

        protected int GetIndexOfStep(int stepId)
        {
            return _steps.FindIndex(step => step.GetStepId() == stepId);
        }

    }
}
