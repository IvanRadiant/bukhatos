﻿using Bukhatos.Domain.SeedWork;
using System;
using System.Collections.Generic;

namespace Bukhatos.Domain.Entities.DocumentAggregate
{
    public class AttributeValue : ValueObject
    {
        private string _value;
        private int _attributeId;

        public AttributeValue() {} //EF

        public AttributeValue(int attributeId, string value)
        {
            _attributeId = attributeId;
            SetValue(value);
        }

        public void SetValue(string value)
        {
            _value = value ?? throw new ArgumentNullException(nameof(value));
        }

        public int GetAttributeId()
        {
            return _attributeId;
        }

        public string GetAttributeValue()
        {
            return _value;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return _value;
            yield return _attributeId;
        }
    }
}
