﻿using Bukhatos.Domain.SeedWork;
using System.Threading.Tasks;

namespace Bukhatos.Domain.Entities.DocumentAggregate
{
    public interface IDocumentRepository : IRepository<Document>
    {
        void Update(Document document);
        Document Add(Document document);
        Task<Document> GetAsync(int documentId);
    }
}
