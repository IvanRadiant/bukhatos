﻿using Bukhatos.Domain.Exceptions;
using Bukhatos.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bukhatos.Domain.Entities.DocumentAggregate
{
    public class StepStatus : Enumeration
    {
        public static readonly StepStatus Initialized = new StepStatus(1, nameof(Initialized).ToLowerInvariant());
        public static readonly StepStatus Pending = new StepStatus(2, nameof(Pending).ToLowerInvariant());
        public static readonly StepStatus Approved = new StepStatus(3, nameof(Approved).ToLowerInvariant());
        public static readonly StepStatus Rejected = new StepStatus(4, nameof(Rejected).ToLowerInvariant());

        public StepStatus() {} //EF

        public StepStatus(int id, string name) : base(id, name) { }

        public static IEnumerable<StepStatus> List() => new[] { Pending, Approved, Rejected };

        public static StepStatus FromName(string name)
        {
            var state = List()
                            .SingleOrDefault(
                                s => String.Equals(s.Name, name, StringComparison.CurrentCultureIgnoreCase)
                            );

            return state ?? throw new BukhatosDomainException($"Possible values for StepStatus: {String.Join(",", List().Select(s => s.Name))}"); ;
        }

    }
}