﻿using System;
using System.Collections.Generic;
using Bukhatos.Domain.SeedWork;

namespace Bukhatos.Domain.Entities.DocumentAggregate
{
    public class Comment : ValueObject
    {
        private string _text;
        private string _attachedFile; // Should we create a?

        public Comment() {} //EF
        public Comment(string text, string attachedFile)
        {
            _text = !string.IsNullOrEmpty(text) ? text : throw new ArgumentNullException(nameof(text));
            _attachedFile = attachedFile;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return _text;
            yield return _attachedFile;
        }
    }
}
