﻿using Bukhatos.Domain.Exceptions;
using Bukhatos.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bukhatos.Domain.Entities.DocumentAggregate
{
    public class DocumentStatus : Enumeration
    {
        public static readonly DocumentStatus Created = new DocumentStatus(1, nameof(Created).ToLowerInvariant());
        public static readonly DocumentStatus Pending = new DocumentStatus(2, nameof(Pending).ToLowerInvariant());
        public static readonly DocumentStatus Approved = new DocumentStatus(3, nameof(Approved).ToLowerInvariant());
        public static readonly DocumentStatus Rejected = new DocumentStatus(4, nameof(Rejected).ToLowerInvariant());
        public static readonly DocumentStatus Canceled = new DocumentStatus(5, nameof(Canceled).ToLowerInvariant());
        public static readonly DocumentStatus Deleted = new DocumentStatus(6, nameof(Deleted).ToLowerInvariant());

        public DocumentStatus() {} //EF

        public DocumentStatus(int id, string name) : base(id, name) { }

        public static IEnumerable<DocumentStatus> List() => new[] { Created, Pending, Approved, Rejected, Canceled, Deleted };

        public static DocumentStatus FromName(string name)
        {
            var state = List()
                        .SingleOrDefault(
                            s => String.Equals(s.Name, name, StringComparison.CurrentCultureIgnoreCase)
                        );

            return state ?? throw new BukhatosDomainException($"Possible values for DocumentStatus: {String.Join(",", List().Select(s => s.Name))}"); ;
        }

    }
}
