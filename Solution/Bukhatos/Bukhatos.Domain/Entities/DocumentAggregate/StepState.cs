﻿using Bukhatos.Domain.Events;
using Bukhatos.Domain.SeedWork;
using System;
using System.Collections.Generic;

namespace Bukhatos.Domain.Entities.DocumentAggregate
{
    public class StepState : ValueObject
    {
        private int _stepId;
        private int? _changedById;
        private StepStatus _stepStatus;
        private Comment _comment;

        public StepState() {} //EF

        public StepState(int stepId, int? changedById, StepStatus stepStatus)
        {
            _stepId = stepId;
            _changedById = changedById;
            _stepStatus = stepStatus;
        }

        public StepState(int stepId, int changedById, StepStatus stepStatus, Comment comment) 
            : this(stepId, changedById, stepStatus)
        {
            _comment = comment ?? throw new ArgumentNullException(nameof(comment));
        }

        public StepStatus GetStatus()
        {
            return _stepStatus;
        }

        public int GetStepId()
        {
            return _stepId;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return _stepId;
            yield return _changedById;
            yield return _stepStatus;
            yield return _comment;
        }
    }
}
