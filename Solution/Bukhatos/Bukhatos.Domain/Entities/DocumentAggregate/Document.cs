﻿using System;
using System.Collections.Generic;
using Bukhatos.Domain.Events.Document;
using Bukhatos.Domain.Exceptions;
using Bukhatos.Domain.SeedWork;

namespace Bukhatos.Domain.Entities.DocumentAggregate
{
    /// <summary>
    /// Допилить события предметно области
    /// </summary>
    public class Document : Entity, IAggregateRoot
    {
        private int _createdById;
        private DateTime _createdAt;
        private int _documentTypeId;
        private DocumentStatus _documentStatus;
        //private int _number -- I'm not sure about this field. We need to think about how to form the number of the document.
        private ApprovalProcess _approvalProcess;
        private List<AttributeValue> _attributes;

        public IEnumerable<AttributeValue> Attributes => _attributes.AsReadOnly();

        //EF
        public Document()
        {
            _attributes = new List<AttributeValue>();
        } 

        /// <summary>
        /// Initializes the document without approval process.
        /// </summary>
        /// <param name="createdBy">who created the document</param>
        /// <param name="documentTypeId">which type of document it is</param>
        public Document(int createdById, int documentTypeId, int[] attributesIds)
        {
            _createdById = createdById;
            _createdAt = DateTime.Now;
            _documentTypeId = documentTypeId;
            _documentStatus = DocumentStatus.Created;
            InitAttributes(attributesIds ?? throw new ArgumentNullException(nameof(attributesIds)));

            AddDomainEvent(new DocumentCreatedDomainEvent(this.Id, createdById));
        }

        /// <summary>
        /// Initializes the document with the approval process.
        /// </summary>
        /// <param name="approvaProcess">the approval process</param>
        /// <param name="createdBy">who created the document</param>
        /// <param name="documentTypeId">which type of document it is</param>
        public Document(int createdById, int documentTypeId, int[] attributes, ApprovalProcess approvalProcess) : this(createdById, documentTypeId, attributes)
        {
            _approvalProcess = approvalProcess;
        }

        public void SetValueToAttributeOfId(int attributeId, string value)
        {
            if (_documentStatus == DocumentStatus.Pending || _documentStatus == DocumentStatus.Approved)
                return;

            if (value == null)
                throw new ArgumentNullException(nameof(value));

            var index = GetIndexOfAttribute(attributeId);

            if (index < 0)
                throw new BukhatosDomainException($"The document doesn't contain any attribute with id = {attributeId}");

            AttributeValue attribute = new AttributeValue(attributeId, value);
            AttributeValue oldValue = _attributes[index];
            _attributes[index] = attribute;

            AddDomainEvent(new AttributeValueChangedDomainEvent(this.Id, attributeId, value, oldValue.GetAttributeValue()));
        }

        public void SendForApproval(int sentById)
        {
            if (_approvalProcess == null) return;

            if (_documentStatus == DocumentStatus.Approved || _documentStatus == DocumentStatus.Pending)
                throw new BukhatosDomainException($"It is not possible to send the document for approval due to it has already been in status {_documentStatus}");

            _approvalProcess.Start();
            _documentStatus = DocumentStatus.Pending;

            AddDomainEvent(new DocumentSentForApprovalDomainEvent(this.Id, sentById));
        }

        public void Approve(int stepId, int approvedById)
        {
            if (_approvalProcess == null) return;

            if (_documentStatus != DocumentStatus.Pending)
                throw new BukhatosDomainException($"It is not possible to approve the document due to it has already been in status {_documentStatus}");

            _approvalProcess.ApproveStep(stepId, approvedById);

            AddDomainEvent(new DocumentStepApprovedDomainEvent(this.Id, stepId, approvedById));

            if(_approvalProcess.IsApproved())
            {
                _documentStatus = DocumentStatus.Approved;
                AddDomainEvent(new DocumentApprovedDomainEvent(this.Id, _createdById));
            }
        }

        public void Reject(int stepId, int rejetedById, string comment, string attachedFile)
        {
            if (_approvalProcess == null) return;

            if (_documentStatus != DocumentStatus.Pending)
                throw new BukhatosDomainException($"It is not possible to reject approvement of the document due to it has already been in status {_documentStatus}");

            _approvalProcess.RejectStep(stepId, rejetedById, comment, attachedFile);
            _documentStatus = DocumentStatus.Rejected;

            AddDomainEvent(new DocumentRejectedDomainEvent(this.Id, stepId, rejetedById));
        }

        public void CancelApproving(int canceledById)
        {
            if (_approvalProcess == null) return;

            if (_documentStatus != DocumentStatus.Pending)
                throw new BukhatosDomainException($"It is not possible to cancel approvement of the document due to it has already been in status {_documentStatus}");

            _documentStatus = DocumentStatus.Canceled;

            AddDomainEvent(new DocumentCanceledDomainEvent(this.Id, canceledById));
        }

        public DocumentStatus GetStatus()
        {
            return _documentStatus;
        }

        protected void InitAttributes(int[] attributesIds)
        {
            _attributes = new List<AttributeValue>();

            for(int i = 0; i < attributesIds.Length; i++)
            {
                var attribute = new AttributeValue(attributesIds[i], String.Empty);
                _attributes.Add(attribute);
            }
        }

        protected int GetIndexOfAttribute(int attributeId)
        {
            return _attributes.FindIndex(attribute => attribute.GetAttributeId() == attributeId);
        }

    }
}
