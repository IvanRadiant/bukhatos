﻿using System;
using Bukhatos.Domain.SeedWork;

namespace Bukhatos.Domain.Entities.PermissionAggregate
{
    public class Permission : Entity, IAggregateRoot
    {       
        private string _name;

        public Permission() { } //EF

        public Permission(string name)
        {
            ChangeName(name);
        }

        public void ChangeName(string name)
        {
            if (string.IsNullOrEmpty(name))
                 throw new ArgumentNullException(nameof(name));

            _name = name;
        }
      
    }
}
