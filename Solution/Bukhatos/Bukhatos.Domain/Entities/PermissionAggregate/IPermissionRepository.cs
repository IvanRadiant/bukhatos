﻿using Bukhatos.Domain.SeedWork;
using System.Threading.Tasks;

namespace Bukhatos.Domain.Entities.PermissionAggregate
{
    public interface IPermissionRepository : IRepository<Permission>
    {
        void Update(Permission permission);
        Permission Add(Permission permission);
        void Remove(Permission permission);
        Task<Permission> GetAsync(int permissionId);

    }
}
