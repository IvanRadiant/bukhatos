﻿using Bukhatos.Domain.Entities.ApprovalSchemeAggregate;
using Bukhatos.Domain.Entities.DocumentAggregate;
using Bukhatos.Domain.Exceptions;
using Bukhatos.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bukhatos.Domain.Entities.DocumentTypeAggregate
{
    public class DocumentType : Entity, IAggregateRoot
    {
        private string _name;
        private ApprovalScheme _approvalScheme;
        private List<Attribute> _attributes;

        public IEnumerable<Attribute> Attributes => _attributes.AsReadOnly();

        //EF
        public DocumentType()
        {
            _attributes = new List<Attribute>();
        }

        public DocumentType(string name) : this()
        {
            ChangeName(name);
        }

        public void ChangeName(string name)
        {
            if (String.IsNullOrEmpty(name))
                throw new ArgumentNullException(nameof(name));

            _name = name;
        }

        public Document CreateNewDocument(int createdById)
        {
            Document document = null;
            var attributesIds = _attributes.Select(attr => attr.GetId()).ToArray();

            if (_approvalScheme == null)
                document = new Document(createdById, this.Id, attributesIds);
            else
            {
                ApprovalProcess approvalProcess = _approvalScheme.CreateApprovalProcess();
                document = new Document(createdById, this.Id, attributesIds, approvalProcess);
            }

            return document;
        }

        public void SetApprovalScheme(ApprovalScheme approvalScheme)
        {
            _approvalScheme = approvalScheme ?? throw new ArgumentNullException(nameof(approvalScheme));
        }

        public void AttachAttribute(int attributeId)
        {
            var attribute = new Attribute(attributeId);

            var exists = _attributes.SingleOrDefault(attr => attr.Equals(attribute));

            if (exists != null)
                throw new BukhatosDomainException("The attribute has alredy attached to this template.");

            _attributes.Add(attribute);
        }

        public void DetachAttribute(int attributeId)
        {
            var attribute = new Attribute(attributeId);

            var exists = _attributes.SingleOrDefault(attr => attr.Equals(attribute));

            if (exists == null)
                throw new BukhatosDomainException("This template doesn't contain the attribute.");

            _attributes.Remove(attribute);
        }

    }
}
