﻿using System.Collections.Generic;
using Bukhatos.Domain.SeedWork;

namespace Bukhatos.Domain.Entities.DocumentTypeAggregate
{
    public class Attribute : ValueObject
    {
        private int _attributeId;

        public Attribute() {} //EF

        public Attribute(int attributeId)
        {
            _attributeId = attributeId;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return _attributeId;
        }

        public int GetId()
        {
            return _attributeId;
        }
    }
}
