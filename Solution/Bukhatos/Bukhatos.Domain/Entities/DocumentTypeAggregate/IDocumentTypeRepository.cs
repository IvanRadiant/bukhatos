﻿using Bukhatos.Domain.SeedWork;
using System.Threading.Tasks;

namespace Bukhatos.Domain.Entities.DocumentTypeAggregate
{
    public interface IDocumentTypeRepository : IRepository<DocumentType>
    {
        void Update(DocumentType documentType);
        DocumentType Add(DocumentType documentType);
        Task<DocumentType> GetAsync(int documentTypeId);
    }
}
