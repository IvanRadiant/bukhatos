﻿using Bukhatos.Domain.Exceptions;
using Bukhatos.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bukhatos.Domain.Entities.DocumentAttributeAggregate
{
    public class AttributeType : Enumeration
    {
        public static readonly AttributeType DigitalField = new AttributeType(1, nameof(DigitalField).ToLowerInvariant());
        public static readonly AttributeType StringField = new AttributeType(2, nameof(StringField).ToLowerInvariant());
        public static readonly AttributeType DatetimeField = new AttributeType(3, nameof(DatetimeField).ToLowerInvariant());
        public static readonly AttributeType MoneyField = new AttributeType(4, nameof(MoneyField).ToLowerInvariant());
        public static readonly AttributeType TextField = new AttributeType(5, nameof(MoneyField).ToLowerInvariant());

        public AttributeType() {} //EF

        public AttributeType(int id, string name) : base(id, name) { }

        public static IEnumerable<AttributeType> List() => new[] { DigitalField, StringField, DatetimeField, MoneyField, TextField };

        //might take this method to the Enumeration
        public static AttributeType FromName(string name)
        {
            var state = List()
                        .SingleOrDefault(
                            s => String.Equals(s.Name, name, StringComparison.CurrentCultureIgnoreCase)
                        );

            return state ?? throw new BukhatosDomainException($"Possible values for AttributeType: {String.Join(",", List().Select(s => s.Name))}"); ;
        }
    }
}
