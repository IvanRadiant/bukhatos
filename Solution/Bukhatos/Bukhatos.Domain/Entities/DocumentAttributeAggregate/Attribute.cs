﻿using Bukhatos.Domain.SeedWork;
using System;

namespace Bukhatos.Domain.Entities.DocumentAttributeAggregate
{
    public class Attribute : Entity, IAggregateRoot
    {
        private string _name;
        private AttributeType _type;

        public Attribute() {} //EF

        public Attribute(string name, AttributeType type)
        {
            ChangeAttributeName(name);
            ChangeAttributeType(type);
        }

        public void ChangeAttributeName(string name)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentNullException(nameof(name));

            _name = name;
        }

        public string GetName()
        {
            return _name;
        }

        public void ChangeAttributeType(AttributeType type)
        {
            _type = type ?? throw new ArgumentNullException(nameof(type));
        }
    }
}
