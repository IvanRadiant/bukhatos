﻿using Bukhatos.Domain.SeedWork;
using System.Threading.Tasks;

namespace Bukhatos.Domain.Entities.DocumentAttributeAggregate
{
    public interface IAttributeRepository : IRepository<Attribute>
    {
        void Update(Attribute attribute);
        void Remove(Attribute attribute);
        Attribute Add(Attribute attribute);
        Task<Attribute> GetAsync(int attributeId);
    }
}
