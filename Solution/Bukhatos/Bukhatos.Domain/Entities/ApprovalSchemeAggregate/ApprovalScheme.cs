﻿using Bukhatos.Domain.Entities.DocumentAggregate;
using Bukhatos.Domain.Exceptions;
using Bukhatos.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bukhatos.Domain.Entities.ApprovalSchemeAggregate
{
    public class ApprovalScheme : Entity, IAggregateRoot
    {
        private string _name;
        private List<Step> _steps;

        public IEnumerable<Step> Steps => _steps.AsReadOnly();
        
        //EF
        public ApprovalScheme()
        {
            _steps = new List<Step>();
        }

        public ApprovalScheme(string name) : this()
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentNullException(nameof(name));

            _name = name;
        }

        public void AddStep(string name, int? roleId, int? nextStepId)
        {
            Step step = new Step(name, roleId, nextStepId);
            _steps.Add(step);
        }

        public void EditStep(int stepId, string name, int? roleId, int? nextStepId)
        {
            Step step = GetStepOfId(stepId);

            step.ChangeName(name);
            step.ChangeResponsibleRole(roleId);
            step.ChangeNextStep(nextStepId);
        }

        public void RemoveStep(int stepId)
        {
            Step step = GetStepOfId(stepId);
            _steps.Remove(step);
        }

        public ApprovalProcess CreateApprovalProcess()
        {
            int[] stepsIds = GetStepsIds();
            
            return new ApprovalProcess(stepsIds);
        }

        protected Step GetStepOfId(int stepId)
        {
            Step step = _steps.SingleOrDefault(st => st.Id == stepId);
            return step ?? throw new BukhatosDomainException("Step was not found.");
        }

        protected int[] GetStepsIds()
        {
            return _steps.Select(step => step.Id).ToArray();
        }
    }
}
