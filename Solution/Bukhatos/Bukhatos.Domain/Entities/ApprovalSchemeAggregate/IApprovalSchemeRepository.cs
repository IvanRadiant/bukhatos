﻿using Bukhatos.Domain.SeedWork;
using System.Threading.Tasks;

namespace Bukhatos.Domain.Entities.ApprovalSchemeAggregate
{
    public interface IApprovalSchemeRepository : IRepository<ApprovalScheme>
    {
        void Update(ApprovalScheme approvalScheme);
        void Remove(ApprovalScheme approvalScheme);
        ApprovalScheme Add(ApprovalScheme approvalScheme);
        Task<ApprovalScheme> GetAsync(int approvalSchemeId);
    }
}
