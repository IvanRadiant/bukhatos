﻿using Bukhatos.Domain.Entities.DocumentAggregate;
using Bukhatos.Domain.SeedWork;
using System;

namespace Bukhatos.Domain.Entities.ApprovalSchemeAggregate
{
    public class Step : Entity
    {
        private string _name;
        private int? _roleId;
        private int? _nextStepId;

        public Step() {} //EF

        public Step(string name, int? roleId, int? nextStepId)
        {
            ChangeName(name);
            _nextStepId = nextStepId;
            _roleId = roleId;
        }

        public void ChangeName(string name)
        {
            if (String.IsNullOrEmpty(name))
                throw new ArgumentNullException(nameof(name));

            _name = name;
        }

        public void ChangeResponsibleRole(int? roleId)
        {
            _roleId = roleId;
        }

        public void ChangeNextStep(int? nextStepId)
        {
            _nextStepId = nextStepId;
        }
    }
}
