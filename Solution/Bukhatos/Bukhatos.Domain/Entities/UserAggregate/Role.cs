﻿using System.Collections.Generic;
using Bukhatos.Domain.SeedWork;

namespace Bukhatos.Domain.Entities.UserAggregate
{
    public class Role : ValueObject
    {
        private int _roleId;

        public Role() { } //EF

        public Role(int roleId)
        {
            _roleId = roleId;
        }

        public int GetId()
        {
            return _roleId;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return _roleId;
        }
    }
}
