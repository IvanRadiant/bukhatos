﻿using Bukhatos.Domain.SeedWork;
using System;
using System.Collections.Generic;

namespace Bukhatos.Domain.Entities.UserAggregate
{
    public class GlobalUserInfo : ValueObject
    {
        private string _DAS;
        private string _personalNum;
        private string _department;
        private string _tabelNum;
        private string _MB3;

        public GlobalUserInfo() { } //EF

        public GlobalUserInfo(string das, string personalNum, string department, string tabelNum, string mb3)
        {
            _DAS = !string.IsNullOrEmpty(das) ? das : throw new ArgumentNullException(nameof(das));
            _personalNum = !string.IsNullOrEmpty(personalNum) ? personalNum : throw new ArgumentNullException(nameof(personalNum));
            _department = !string.IsNullOrEmpty(department) ? department : throw new ArgumentNullException(nameof(department));
            _tabelNum = !string.IsNullOrEmpty(tabelNum) ? tabelNum : throw new ArgumentNullException(nameof(tabelNum));
            _MB3 = !string.IsNullOrEmpty(mb3) ? mb3 : throw new ArgumentNullException(nameof(mb3));
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return _DAS;
            yield return _personalNum;
            yield return _department;
            yield return _tabelNum;
            yield return _MB3;
        }
    }
}
