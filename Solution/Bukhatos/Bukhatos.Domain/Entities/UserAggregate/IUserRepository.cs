﻿using Bukhatos.Domain.SeedWork;
using System.Threading.Tasks;

namespace Bukhatos.Domain.Entities.UserAggregate
{
    public interface IUserRepository : IRepository<User>
    {
        void Update(User user);
        User Add(User user);
        Task<User> GetAsync(int UserId);
    }
}
