﻿using System;
using System.Collections.Generic;
using Bukhatos.Domain.SeedWork;
using Bukhatos.Domain.Exceptions;
using System.Linq;

namespace Bukhatos.Domain.Entities.UserAggregate
{
    public class User : Entity, IAggregateRoot
    {
        private string _userName;
        private FIO _userFio;
        private GlobalUserInfo _globalUserInfo;
        private List<Role> _roles;

        public IEnumerable<Role> Roles => _roles.AsReadOnly();


        //EF
        public User()
        {
            _roles = new List<Role>();
        } 

        public User(string userName, FIO fio, GlobalUserInfo globalUserInfo)
        {
            _userName = !string.IsNullOrEmpty(userName) ? userName : throw new ArgumentNullException(nameof(userName));
            _userFio = fio ?? throw new ArgumentNullException(nameof(fio));
            _globalUserInfo = globalUserInfo ?? throw new ArgumentNullException(nameof(globalUserInfo));
            _roles = new List<Role>();
        }

        public void ChangeGlobalUserInfo(GlobalUserInfo globalUserInfo)
        {
            _globalUserInfo = globalUserInfo ?? throw new ArgumentNullException(nameof(globalUserInfo));
        }
            
        public void AddRole(int roleId)
        {
            Role role = GetRoleById(roleId);

            if (role != null)
                throw new BukhatosDomainException("User already has had this role");

            role = new Role(roleId);
            _roles.Add(role);
        }

        public void RemoveRole(int roleId)
        {
            Role role = GetRoleById(roleId);

            if (role == null)
                throw new BukhatosDomainException("User doesn't have the role");

            _roles.Remove(role);
        }

        protected Role GetRoleById(int roleId)
        {
            Role role = _roles.SingleOrDefault(r => r.GetId() == roleId);
            return role;
        }

       public bool HasRole(int roleId)
        {
            Role role = GetRoleById(roleId);

            return role != null ? true : false;
        }

        public GlobalUserInfo GetGlobaluserInfo()
        {
            return _globalUserInfo;
        }

        public FIO GetFIO()
        {
            return _userFio;
        }



    }
}
