﻿using System;
using System.Collections.Generic;
using Bukhatos.Domain.SeedWork;

namespace Bukhatos.Domain.Entities.UserAggregate
{
    public class FIO : ValueObject
    {
        private string _firstName;
        private string _secondName;
        private string _patronymic;

        private string _fullName;

        public FIO() { } //EF

        public FIO(string firstName, string secondName, string patronymic)
        {
            _firstName = !string.IsNullOrWhiteSpace(firstName) ? firstName : throw new ArgumentNullException(nameof(firstName));
            _secondName = !string.IsNullOrWhiteSpace(secondName) ? secondName : throw new ArgumentNullException(nameof(secondName));
            _patronymic = !string.IsNullOrWhiteSpace(patronymic) ? patronymic : null;
            _fullName = _firstName + " " + _secondName + " " + _patronymic ?? "";
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return _firstName;
            yield return _secondName;
            yield return _patronymic;
        }

        public override string ToString()
        {
            return _fullName;
        }

    }
}
