﻿using Bukhatos.Domain.SeedWork;
using System.Threading.Tasks;

namespace Bukhatos.Domain.Entities.RoleAggregate
{
    public interface IRoleRepository : IRepository<Role>
    {
        void Update(Role role);
        Role Add(Role role);
        void Remove(Role role);
        Task<Role> GetAsync(int RoleId);
    }
}
