﻿using System.Collections.Generic;
using Bukhatos.Domain.SeedWork;

namespace Bukhatos.Domain.Entities.RoleAggregate
{
    public class Permission : ValueObject
    {
        private int _permissionId;

        public Permission() { } //EF

        public Permission(int permissionId)
        {
            _permissionId = permissionId;
        }

        public int GetId()
        {
            return _permissionId;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return _permissionId;
        }
    }
}
