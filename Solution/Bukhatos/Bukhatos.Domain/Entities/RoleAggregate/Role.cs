﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bukhatos.Domain.Exceptions;
using Bukhatos.Domain.SeedWork;

namespace Bukhatos.Domain.Entities.RoleAggregate
{
    public class Role : Entity, IAggregateRoot
    {
        private string _name;
        private List<Permission> _permissions;

        public IEnumerable<Permission> Permissions => _permissions.AsReadOnly();

        //EF
        public Role()
        {
            _permissions = new List<Permission>();
        }

        public Role(string name) : this()
        {
            ChangeName(name);
        }

        public void ChangeName(string name)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentNullException(nameof(name));

            _name = name;
        }

        public void AddPermission(int permissionId)
        {
            var exists = _permissions.SingleOrDefault(permis => permis.GetId() == permissionId);

            if(exists != null)
                throw new BukhatosDomainException("Permission already has been added");

            var permission = new Permission(permissionId);
            _permissions.Add(permission);
        }

        public void RemovePermission(int permissionId)
        {
            var permission = _permissions.SingleOrDefault(permis => permis.GetId() == permissionId);

            if (permission == null)
                throw new BukhatosDomainException("This role doesn't contain the permission");

            _permissions.Remove(permission);
        }
    }
}
