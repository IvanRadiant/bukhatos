﻿using MediatR;

namespace Bukhatos.Domain.Events.Document
{
    public class DocumentSentForApprovalDomainEvent : INotification
    {
        public int DocId { get; }
        public int SentById { get; }
        
        public DocumentSentForApprovalDomainEvent(int docId, int sentById)
        {
            DocId = docId;
            SentById = SentById;
        }
    }
}
