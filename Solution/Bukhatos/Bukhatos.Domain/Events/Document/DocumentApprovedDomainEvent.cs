﻿using MediatR;

namespace Bukhatos.Domain.Events.Document
{
    public class DocumentApprovedDomainEvent : INotification
    {
        public int DocId { get; }
        public int CreatedById { get; }
        
        public DocumentApprovedDomainEvent(int docId, int createdById)
        {
            DocId = docId;
            CreatedById = createdById;
        }
    }
}
