﻿using MediatR;

namespace Bukhatos.Domain.Events.Document
{
    public class DocumentStepApprovedDomainEvent : INotification
    {
        public int DocId { get; }
        public int StepId { get; }
        public int ApprovedById { get; }

        public DocumentStepApprovedDomainEvent(int docId, int stepId, int approvedById)
        {
            DocId = docId;
            StepId = stepId;
            ApprovedById = approvedById;
        }
    }
}
