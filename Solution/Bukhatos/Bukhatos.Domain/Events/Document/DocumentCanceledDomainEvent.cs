﻿using MediatR;

namespace Bukhatos.Domain.Events.Document
{
    public class DocumentCanceledDomainEvent : INotification
    {
        public int DocId { get; }
        public int CanceledById { get; }

        public DocumentCanceledDomainEvent(int docId, int canceledById)
        {
            DocId = docId;
            CanceledById = canceledById;
        }
    }
}
