﻿using MediatR;

namespace Bukhatos.Domain.Events.Document
{
    public class DocumentRejectedDomainEvent : INotification
    {
        public int DocId { get; }
        public int StepId { get; }
        public int RejectedById { get; }

        public DocumentRejectedDomainEvent(int docId, int stepId, int rejectedById)
        {
            DocId = docId;
            StepId = stepId;
            RejectedById = rejectedById;
        }
    }
}
