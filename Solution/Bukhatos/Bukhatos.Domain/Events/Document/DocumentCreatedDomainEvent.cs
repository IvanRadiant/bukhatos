﻿using MediatR;

namespace Bukhatos.Domain.Events.Document
{
    public class DocumentCreatedDomainEvent : INotification
    {
        public int DocId { get; }
        public int CreatedById { get; }
  
        public DocumentCreatedDomainEvent(int docId, int createdById)
        {
            DocId = docId;
            CreatedById = createdById;
        }
    }
}
