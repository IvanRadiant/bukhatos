﻿using MediatR;

namespace Bukhatos.Domain.Events.Document
{
    /// <summary>
    /// The handler of this event must be responsible for validating the value of the attribute
    /// </summary>
    public class AttributeValueChangedDomainEvent : INotification
    {
        public int DocId { get; }
        public int AttributeId { get; }
        public string Value { get; }
        public string OldValue { get; }

        public AttributeValueChangedDomainEvent(int docId, int attributeId, string value, string oldValue)
        {
            DocId = docId;
            AttributeId = attributeId;
            Value = value;
            OldValue = oldValue;
        }
    }
}
