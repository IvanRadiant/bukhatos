﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bukhatos.Domain.Exceptions
{
    /// <summary>
    /// Exception type for domain exceptions
    /// </summary>
    public class BukhatosDomainException : Exception
    {
        public BukhatosDomainException()
        { }

        public BukhatosDomainException(string message)
            : base(message)
        { }

        public BukhatosDomainException(string message, Exception innerException)
            : base(message, innerException)
        { }
    }
}
