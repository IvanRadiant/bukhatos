﻿using Autofac;
using Bukhatos.Web.GraphQLModels;
using GraphQL;
using GraphQL.Types;
using MediatR;
using System.Reflection;


namespace Bukhatos.Web.Infrastructure.AutofacModules
{
    public class GraphQLModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(typeof(BukhatosScheme).GetTypeInfo().Assembly)
                   .PublicOnly();

            builder.RegisterType<DocumentExecuter>().As<IDocumentExecuter>();

            builder.Register<IDependencyResolver>(c =>
            {
                var context = c.Resolve<IComponentContext>();
                return new FuncDependencyResolver(type => context.Resolve(type));
            });

            builder.RegisterType<BukhatosScheme>().As<ISchema>();
        }

    }
}
