﻿using Autofac;
using Bukhatos.Domain.Entities.DocumentAggregate;
using Bukhatos.EntityFramework.Repositories;

namespace Bukhatos.Web.Infrastructure.AutofacModules
{
    public class ApplicationModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            /*builder.Register(c => new OrderQueries(QueriesConnectionString))
                .As<IOrderQueries>()
                .InstancePerLifetimeScope();*/

            builder.RegisterType<DocumentRepository>()
                   .As<IDocumentRepository>()
                   .InstancePerLifetimeScope();
        }
    }
}
