﻿using GraphQL.Types;

namespace Bukhatos.Web.GraphQLModels
{
    public class DefaultOutputType : ObjectGraphType<Default>
    {

        public DefaultOutputType()
        {
            Field(d => d.Status);
        }
    }
}
