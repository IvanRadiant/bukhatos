﻿using Bukhatos.Application.Commands;
using Bukhatos.Web.GraphQLModels;
using System;
using GraphQL.Types;
using MediatR;
using Microsoft.Extensions.Logging;

namespace Bukhatos.Web.GraphQLModels
{
    public class BukhatosMutation : ObjectGraphType
    {
        private readonly IMediator _mediator;
        private readonly ILogger<BukhatosMutation> _logger;

        public BukhatosMutation(IMediator mediator, ILogger<BukhatosMutation> logger)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));

            #region GraphQL

            Name = "CreateDocumentMutation";

            FieldAsync<DefaultOutputType>(
                "createDocument",

                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<CreateDocumentInputType>> { Name = "doc" }
                ),

                resolve: async context =>
                {
                    var command = context.GetArgument<CreateDocumentCommand>("doc");
                    await _mediator.Send(command);

                    return new Default()
                    {
                        Status = "OK"
                    };
                });

            #endregion
            
        }
    }
}
