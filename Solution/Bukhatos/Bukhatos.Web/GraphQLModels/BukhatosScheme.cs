﻿using GraphQL;
using GraphQL.Types;

namespace Bukhatos.Web.GraphQLModels
{
    public class BukhatosScheme : Schema
    {
        public BukhatosScheme(IDependencyResolver resolver) : base(resolver)
        {
            Query = resolver.Resolve<BukhatosQuery>();
            Mutation = resolver.Resolve<BukhatosMutation>();
        }
    }
}
