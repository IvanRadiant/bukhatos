﻿using GraphQL.Types;

namespace Bukhatos.Web.GraphQLModels
{
    public class CreateDocumentInputType : InputObjectGraphType
    {
        public CreateDocumentInputType()
        {
            Name = "CreateDocumentInput";

            Field<NonNullGraphType<IntGraphType>>("userId");
            Field<NonNullGraphType<IntGraphType>>("docTypeId");
        }
    }
}
