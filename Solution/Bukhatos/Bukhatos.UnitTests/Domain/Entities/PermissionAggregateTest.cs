﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Bukhatos.Domain.Entities.PermissionAggregate;

namespace Bukhatos.UnitTests.Domain.Entities
{
    public class PermissionAggregateTest
    {
        [Fact]
        public void Create_permission_success()
        {
            //Arrange
            var name = "Permission1";

            //Act
            Permission permission = new Permission(name);

            //Assert
            Assert.NotNull(permission);
        }

        [Fact]
        public void Create_permission_when_name_is_null_or_empty()
        {
            //Arrange
            var name = String.Empty;

            //Act-Assert
            Assert.Throws<ArgumentNullException>(() => new Permission(name));
        }

        [Fact]
        public void Change_name_of_permission_fail()
        {
            //Arrange
            var name = String.Empty;
            Permission permission = new Permission("test");

            //Act-Assert
            Assert.Throws<ArgumentNullException>(() => permission.ChangeName(name));
        }

    }
}
