﻿using System;
using System.Collections.Generic;
using Bukhatos.Domain.Entities.UserAggregate;
using Bukhatos.Domain.Exceptions;
using Xunit;

namespace Bukhatos.UnitTests.Domain.Entities
{
    public class UserAggregateTest
    {
        [Fact]
        public void Create_user_with_success_data()
        {
            //Arrange
            string userName = "userName";
            string firstname = "testName";
            string secondName = "testName";
            string patronymic = "testName";
            FIO fio = new FIO(firstname, secondName, patronymic);
            string das = "FG649";
            string personalNum = "dfg34";
            string department = "XX";
            var tableNum = "34df";
            string mb3 = "xxx2";
            GlobalUserInfo globalUserInfo = new GlobalUserInfo(das, personalNum, department, tableNum, mb3);

            //Act
            User user = new User(userName, fio, globalUserInfo);

            //Assert
            Assert.NotNull(user);
        }

        [Fact]
        public void Create_user_with_nullable_data()
        {
            //Arrange
            string firstname = string.Empty;
            FIO fio = null;
            GlobalUserInfo globalUserInfo = null;

            //Act-Assert
            Assert.Throws<ArgumentNullException>(() => new User(firstname, fio, globalUserInfo));
        }

        [Fact]
        public void Change_global_info_fail()
        {
            //Arrange
            User user = new User();

            //Act-Assert
            Assert.Throws<ArgumentNullException>(() => user.ChangeGlobalUserInfo(null));
        }

        [Fact]
        public void Add_role_to_user_success()
        {
            //Arrange
            User user = new User();
            var Expected_count_of_elements = 1;
            var roleId = 1;

            //Act
            user.AddRole(roleId);
            
            //Assert
            Assert.Equal(Expected_count_of_elements, ((IReadOnlyCollection<Role>)user.Roles).Count);
        }

        [Fact]
        public void Add_role_to_user_fail_already_added()
        {
            //Arrange
            User user = new User();
            var roleId = 53;
            user.AddRole(roleId);


            //Assert - Act
            Assert.Throws<BukhatosDomainException>(() => user.AddRole(roleId));
        }

        [Fact]
        public void Delete_permission_success()
        {
            //Arrange
            User user = new User();
            var roleId = 55;

            //Act
            user.AddRole(roleId);
            user.RemoveRole(roleId);

            //Assert
            Assert.DoesNotContain(user.Roles, (x => x.GetId() == roleId));
        }

        [Fact]
        public void Delete_permission_fail()
        {
            //Arrange
            User user = new User();
            var roleId = 55;
            var another_roleId = 100;

            //Act
            user.AddRole(roleId);

            //Assert
            Assert.Throws<BukhatosDomainException>(() => user.RemoveRole(another_roleId));
        }
        
    }
}
