﻿using Bukhatos.Domain.Entities.ApprovalSchemeAggregate;
using System;
using System.Collections.Generic;
using Xunit;

namespace Bukhatos.UnitTests.Domain.Entities
{
    public class ApprovalSchemeAggregateTest
    {
        [Fact]
        public void Create_approval_scheme_success()
        {
            //Arrange
            var name = "approvalScheme";

            //Act
            var fakeApprovalScheme = new ApprovalScheme(name);

            //Assert
            Assert.NotNull(fakeApprovalScheme);
        }

        [Fact]
        public void Create_approval_scheme_fail_when_name_is_null_or_empty()
        {
            //Arrange
            var name = String.Empty;

            //Act-Assert
            Assert.Throws<ArgumentNullException>(() => new ApprovalScheme(name));
        }

        [Fact]
        public void Create_step_success()
        {
            //Arrange
            var name = "step";

            //Act
            var fakeStep = new Step(name, null, null);

            //Assert
            Assert.NotNull(fakeStep);
        }

        [Fact]
        public void Create_step_fail_when_name_is_null_or_empty()
        {
            //Arrange
            var name = String.Empty;

            //Act-Assert
            Assert.Throws<ArgumentNullException>(() => new Step(name, null, null));
        }

        [Fact]
        public void Change_step_name_fail_when_name_is_null_or_empty()
        {
            //Arrange
            var name = "stepName";
            string newName = null;
            var step = new Step(name, null, null);

            //Act-Assert
            Assert.Throws<ArgumentNullException>(() => step.ChangeName(newName));
        }

        [Fact]
        public void Add_step_success()
        {
            //Arrange
            var stepName = "step";
            var approvalSchemeName = "approvalScheme";
            var approvalScheme = new ApprovalScheme(approvalSchemeName);
            var expectedResult = 1;

            //Act
            approvalScheme.AddStep(stepName, null, null);

            //Assert
            Assert.Equal(expectedResult, ((IReadOnlyCollection<Step>)approvalScheme.Steps).Count);
        }

        [Fact]
        public void Create_approval_process_success()
        {
            //Arrange
            var stepName = "step";
            var approvalSchemeName = "approvalScheme";

            var approvalScheme = new ApprovalScheme(approvalSchemeName);
            approvalScheme.AddStep(stepName, null, null);

            //Act
            var approvalProcess = approvalScheme.CreateApprovalProcess();

            //Assert
            Assert.NotNull(approvalProcess);
        }
    }
}
