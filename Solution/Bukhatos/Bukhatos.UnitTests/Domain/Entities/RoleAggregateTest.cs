﻿using System;
using System.Collections.Generic;
using Xunit;
using Bukhatos.Domain.Entities.RoleAggregate;
using Bukhatos.Domain.Exceptions;

namespace Bukhatos.UnitTests.Domain.Entities
{
    public class RoleAggregateTest
    {
        [Fact]
        public void Create_role_success()
        {
            //Arrange
            var name = "Role1";

            //Act
            Role role = new Role(name);

            //Assert
            Assert.NotNull(role);
        }

        [Fact]
        public void Create_role_when_name_is_null_or_empty()
        {
            //Arrange
            var name = String.Empty;

            //Act-Assert
            Assert.Throws<ArgumentNullException>(() => new Role(name));
        }

        [Fact]
        public void Change_name_of_role_fail()
        {
            //Arrange
            var name = String.Empty;
            Role role = new Role("role1");

            //Act-Assert
            Assert.Throws<ArgumentNullException>(() => role.ChangeName(name));
        }

        [Fact]
        public void Add_permission_to_role_success()
        {
            //Arrange
            Role role = new Role("role1");
            var Expected_count_of_elements = 1;

            //Act
            role.AddPermission(53);

            //Assert
            Assert.Equal(Expected_count_of_elements, ((IReadOnlyCollection<Permission>)role.Permissions).Count);
        }

        [Fact]
        public void Add_permission_to_role_fail_already_added()
        {
            //Arrange
            Role role = new Role("role1");
            var permissionId = 53;
            role.AddPermission(permissionId);

            //Act-Assert
            Assert.Throws<BukhatosDomainException>(() => role.AddPermission(permissionId));
        }

        [Fact]
        public void Delete_permission_success()
        {
            //Arrange
            Role role = new Role("Role1");
            var permissionId = 55;

            //Act
            role.AddPermission(permissionId);
            role.RemovePermission(permissionId);

            //Assert
            Assert.DoesNotContain(role.Permissions, (x => x.GetId() == permissionId));
        }

        [Fact]
        public void Delete_permission_fail()
        {
            //Arrange
            Role role = new Role("Role1");
            var permissionId = 55;
            var another_permissionId = 100;
            role.AddPermission(permissionId);

            //Act-Assert
            Assert.Throws<BukhatosDomainException>(() => role.RemovePermission(another_permissionId));
        }
    }
}
