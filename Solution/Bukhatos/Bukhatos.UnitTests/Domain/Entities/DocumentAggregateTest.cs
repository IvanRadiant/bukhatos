﻿using Bukhatos.Domain.Entities.DocumentAggregate;
using Bukhatos.Domain.Exceptions;
using System;
using Xunit;

namespace Bukhatos.UnitTests.Domain.Entities
{
    public class DocumentAggregateTest
    {
        [Fact]
        public void Create_document_success()
        {
            //Arrange
            var createdById = 1;
            var docTypeId = 1;
            var attributesIds = new int[] { 1, 2, 3 };

            //Act
            var document = new Document(createdById, docTypeId, attributesIds);

            //Assert
            Assert.NotNull(document);
        }

        [Fact]
        public void Create_document_raises_new_event()
        {
            //Arrange
            var createdById = 1;
            var docTypeId = 1;
            var attributesIds = new int[] { 1, 2, 3 };
            var expectedResult = 1;

            //Act
            var document = new Document(createdById, docTypeId, attributesIds);

            //Assert
            Assert.Equal(expectedResult, document.DomainEvents.Count);
        }

        [Fact]
        public void Set_value_to_attibute_of_id_fail_when_value_is_null()
        {
            //Arrange
            var createdById = 1;
            var docTypeId = 1;
            var attributesIds = new int[] { 1, 2, 3 };
            var document = new Document(createdById, docTypeId, attributesIds);

            //Act-Assert
            Assert.Throws<ArgumentNullException>(() => document.SetValueToAttributeOfId(1, null));
        }

        [Fact]
        public void Set_value_to_attibute_of_id_fail_when_document_doesnt_contain_attribute()
        {
            //Arrange
            var createdById = 1;
            var docTypeId = 1;
            var attributesIds = new int[] { 1, 2, 3 };
            var document = new Document(createdById, docTypeId, attributesIds);

            //Act-Assert
            Assert.Throws<BukhatosDomainException>(() => document.SetValueToAttributeOfId(4, "value"));
        }

        [Fact]
        public void Set_value_to_attibute_of_id_raises_new_event()
        {
            //Arrange
            var createdById = 1;
            var docTypeId = 1;
            var attributesIds = new int[] { 1, 2, 3 };
            var document = new Document(createdById, docTypeId, attributesIds);
            var expectedResult = 2;

            //Act
            document.SetValueToAttributeOfId(2, "value");

            //Assert
            Assert.Equal(expectedResult, document.DomainEvents.Count);
        }

        [Fact]
        public void Send_document_for_approval_success()
        {
            //Arrange
            var createdById = 1;
            var docTypeId = 1;
            var attributesIds = new int[] { 1, 2, 3 };
            var stepsIds = new int[] { 1, 2, 3 };
            var approvalProcess = new ApprovalProcess(stepsIds);
            var document = new Document(createdById, docTypeId, attributesIds, approvalProcess);
            var sentById = 1;

            //Act
            document.SendForApproval(sentById);

            //Assert
            Assert.Equal(DocumentStatus.Pending, document.GetStatus());
        }

        [Fact]
        public void Send_document_for_approval_raises_new_event()
        {
            //Arrange
            var createdById = 1;
            var docTypeId = 1;
            var attributesIds = new int[] { 1, 2, 3 };
            var stepsIds = new int[] { 1, 2, 3 };
            var approvalProcess = new ApprovalProcess(stepsIds);
            var document = new Document(createdById, docTypeId, attributesIds, approvalProcess);
            var sentById = 1;
            var expectedResult = 2;

            //Act
            document.SendForApproval(sentById);

            //Assert
            Assert.Equal(expectedResult, document.DomainEvents.Count);
        }

        [Fact]
        public void Send_document_for_approval_fail_when_document_is_in_wrong_state()
        {
            //Arrange
            var createdById = 1;
            var docTypeId = 1;
            var attributesIds = new int[] { 1, 2, 3 };
            var stepsIds = new int[] { 1, 2, 3 };
            var sentById = 1;
            var approvalProcess = new ApprovalProcess(stepsIds);
            var document = new Document(createdById, docTypeId, attributesIds, approvalProcess);
            document.SendForApproval(sentById);

            //Act-Assert
            Assert.Throws<BukhatosDomainException>(() => document.SendForApproval(sentById));
        }

        [Fact]
        public void Approve_success()
        {
            //Arrange
            var createdById = 1;
            var docTypeId = 1;
            var attributesIds = new int[] { 1, 2, 3 };
            var stepsIds = new int[] { 1 };
            var sentById = 1;
            var approvedById = 2;
            var approvalProcess = new ApprovalProcess(stepsIds);
            var document = new Document(createdById, docTypeId, attributesIds, approvalProcess);
            document.SendForApproval(sentById);

            //Act
            document.Approve(1, approvedById);

            //Assert
            Assert.Equal(DocumentStatus.Approved, document.GetStatus());
        }

        [Fact]
        public void Approve_fail_when_document_doesnt_contain_step()
        {
            //Arrange
            var createdById = 1;
            var docTypeId = 1;
            var attributesIds = new int[] { 1, 2, 3 };
            var stepsIds = new int[] { 1 };
            var sentById = 1;
            var approvedById = 2;
            var approvalProcess = new ApprovalProcess(stepsIds);
            var document = new Document(createdById, docTypeId, attributesIds, approvalProcess);
            document.SendForApproval(sentById);

            //Act-Assert
            Assert.Throws<BukhatosDomainException>(() => document.Approve(2, approvedById));
        }

        [Fact]
        public void Approve_fail_when_document_is_in_wrong_state()
        {
            //Arrange
            var createdById = 1;
            var docTypeId = 1;
            var attributesIds = new int[] { 1, 2, 3 };
            var stepsIds = new int[] { 1 };
            var approvedById = 2;
            var approvalProcess = new ApprovalProcess(stepsIds);
            var document = new Document(createdById, docTypeId, attributesIds, approvalProcess);

            //Act-Assert
            Assert.Throws<BukhatosDomainException>(() => document.Approve(2, approvedById));
        }

        [Fact]
        public void Approve_raises_new_event()
        {
            //Arrange
            var createdById = 1;
            var docTypeId = 1;
            var attributesIds = new int[] { 1, 2, 3 };
            var stepsIds = new int[] { 1, 2, 3 };
            var sentById = 1;
            var approvedById = 2;
            var expectedResult = 3;
            var approvalProcess = new ApprovalProcess(stepsIds);
            var document = new Document(createdById, docTypeId, attributesIds, approvalProcess);
            document.SendForApproval(sentById);

            //Act
            document.Approve(1, approvedById);

            //Assert
            Assert.Equal(expectedResult, document.DomainEvents.Count);
        }

        [Fact]
        public void Document_gets_approved_when_all_steps_are_approved()
        {
            //Arrange
            var createdById = 1;
            var docTypeId = 1;
            var attributesIds = new int[] { 1, 2, 3 };
            var stepsIds = new int[] { 1 };
            var sentById = 1;
            var approvedById = 2;
            var approvalProcess = new ApprovalProcess(stepsIds);
            var document = new Document(createdById, docTypeId, attributesIds, approvalProcess);
            document.SendForApproval(sentById);

            //Act
            document.Approve(1, approvedById);

            //Assert
            Assert.Equal(DocumentStatus.Approved, document.GetStatus());
        }

        [Fact]
        public void Approve_raises_new_event_when_all_steps_are_approved()
        {
            //Arrange
            var createdById = 1;
            var docTypeId = 1;
            var attributesIds = new int[] { 1, 2, 3 };
            var stepsIds = new int[] { 1 };
            var sentById = 1;
            var approvedById = 2;
            var expectedResult = 4;
            var approvalProcess = new ApprovalProcess(stepsIds);
            var document = new Document(createdById, docTypeId, attributesIds, approvalProcess);
            document.SendForApproval(sentById);

            //Act
            document.Approve(1, approvedById);

            //Assert
            Assert.Equal(expectedResult, document.DomainEvents.Count);
        }

        [Fact]
        public void Cancel_approving_success()
        {
            //Arrange
            var createdById = 1;
            var docTypeId = 1;
            var attributesIds = new int[] { 1, 2, 3 };
            var stepsIds = new int[] { 1 };
            var sentById = 1;
            var cacneledById = 2;
            var approvalProcess = new ApprovalProcess(stepsIds);
            var document = new Document(createdById, docTypeId, attributesIds, approvalProcess);
            document.SendForApproval(sentById);

            //Act
            document.CancelApproving(cacneledById);

            //Assert
            Assert.Equal(DocumentStatus.Canceled, document.GetStatus());
        }

        [Fact]
        public void Cancel_approving_fail_when_document_is_in_wrong_state()
        {
            //Arrange
            var createdById = 1;
            var docTypeId = 1;
            var attributesIds = new int[] { 1, 2, 3 };
            var stepsIds = new int[] { 1 };
            var cacneledById = 2;
            var expectedResult = 3;
            var sentById = 1;
            var approvalProcess = new ApprovalProcess(stepsIds);
            var document = new Document(createdById, docTypeId, attributesIds, approvalProcess);
            document.SendForApproval(sentById);

            //Act
            document.CancelApproving(cacneledById);

            //Assert
            Assert.Equal(expectedResult, document.DomainEvents.Count);
        }

        [Fact]
        public void Cancel_approving_raises_new_event()
        {
            //Arrange
            var createdById = 1;
            var docTypeId = 1;
            var attributesIds = new int[] { 1, 2, 3 };
            var stepsIds = new int[] { 1 };
            var cacneledById = 2;
            var approvalProcess = new ApprovalProcess(stepsIds);
            var document = new Document(createdById, docTypeId, attributesIds, approvalProcess);

            //Act-Assert
            Assert.Throws<BukhatosDomainException>(() => document.CancelApproving(cacneledById));
        }

        [Fact]
        public void Reject_success()
        {
            //Arrange
            var createdById = 1;
            var docTypeId = 1;
            var attributesIds = new int[] { 1, 2, 3 };
            var stepsIds = new int[] { 1, 2, 3 };
            var rejectedById = 2;
            var sentById = 1;
            var comment = "comment";
            var file = "file";
            var approvalProcess = new ApprovalProcess(stepsIds);
            var document = new Document(createdById, docTypeId, attributesIds, approvalProcess);
            document.SendForApproval(sentById);

            //Act
            document.Reject(3, rejectedById, comment, file);

            //Assert
            Assert.Equal(DocumentStatus.Rejected, document.GetStatus());
        }

        [Fact]
        public void Reject_fail_when_document_doesnt_contain_step()
        {
            //Arrange
            var createdById = 1;
            var docTypeId = 1;
            var attributesIds = new int[] { 1, 2, 3 };
            var stepsIds = new int[] { 1, 2, 3 };
            var rejectedById = 2;
            var sentById = 1;
            var comment = "comment";
            var file = "file";
            var approvalProcess = new ApprovalProcess(stepsIds);
            var document = new Document(createdById, docTypeId, attributesIds, approvalProcess);
            document.SendForApproval(sentById);

            //Act-Assert
            Assert.Throws<BukhatosDomainException>(() => document.Reject(4, rejectedById, comment, file));
        }

        [Fact]
        public void Reject_fail_when_document_is_in_wrong_state()
        {
            //Arrange
            var createdById = 1;
            var docTypeId = 1;
            var attributesIds = new int[] { 1, 2, 3 };
            var stepsIds = new int[] { 1, 2, 3 };
            var rejectedById = 2;
            var sentById = 1;
            var comment = "comment";
            var file = "file";
            var approvalProcess = new ApprovalProcess(stepsIds);
            var document = new Document(createdById, docTypeId, attributesIds, approvalProcess);
            document.SendForApproval(sentById);

            //Act-Assert
            Assert.Throws<BukhatosDomainException>(() => document.Reject(4, rejectedById, comment, file));
        }

        [Fact]
        public void Reject_raises_new_event()
        {
            //Arrange
            var createdById = 1;
            var docTypeId = 1;
            var attributesIds = new int[] { 1, 2, 3 };
            var stepsIds = new int[] { 1 };
            var sentById = 1;
            var rejectedById = 2;
            var comment = "comment";
            var file = "file";
            var expectedResult = 3;
            var approvalProcess = new ApprovalProcess(stepsIds);
            var document = new Document(createdById, docTypeId, attributesIds, approvalProcess);
            document.SendForApproval(sentById);

            //Act
            document.Reject(1, rejectedById, comment, file);

            //Assert
            Assert.Equal(expectedResult, document.DomainEvents.Count);
        }

        [Fact]
        public void Send_document_for_approval_after_rejection()
        {
            //Arrange
            var createdById = 1;
            var docTypeId = 1;
            var attributesIds = new int[] { 1, 2, 3 };
            var stepsIds = new int[] { 1 };
            var sentById = 1;
            var rejectedById = 2;
            var comment = "comment";
            var file = "file";
            var approvalProcess = new ApprovalProcess(stepsIds);
            var document = new Document(createdById, docTypeId, attributesIds, approvalProcess);
            document.SendForApproval(sentById);

            //Act
            document.Reject(1, rejectedById, comment, file);
            document.SendForApproval(sentById);

            //Assert
            Assert.Equal(DocumentStatus.Pending, document.GetStatus());
        }

        [Fact]
        public void Send_document_for_approval_after_cancelation()
        {
            //Arrange
            var createdById = 1;
            var docTypeId = 1;
            var attributesIds = new int[] { 1, 2, 3 };
            var stepsIds = new int[] { 1 };
            var sentById = 1;
            var canceledById = 2;
            var approvalProcess = new ApprovalProcess(stepsIds);
            var document = new Document(createdById, docTypeId, attributesIds, approvalProcess);
            document.SendForApproval(sentById);

            //Act
            document.CancelApproving(canceledById);
            document.SendForApproval(sentById);

            //Assert
            Assert.Equal(DocumentStatus.Pending, document.GetStatus());
        }
    }
}
