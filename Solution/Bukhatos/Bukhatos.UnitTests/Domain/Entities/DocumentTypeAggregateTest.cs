﻿using Bukhatos.Domain.Entities.ApprovalSchemeAggregate;
using Bukhatos.Domain.Entities.DocumentTypeAggregate;
using Bukhatos.Domain.Exceptions;
using System;
using System.Collections.Generic;
using Xunit;
using BukhatosDocumentTypeAttribute = Bukhatos.Domain.Entities.DocumentTypeAggregate.Attribute;

namespace Bukhatos.UnitTests.Domain.Entities
{
    public class DocumentTypeAggregateTest
    {
        [Fact]
        public void Create_document_type_success()
        {
            //Arrange
            var name = "documentType";

            //Act
            var documentType = new DocumentType(name);

            //Assert
            Assert.NotNull(documentType);
        }

        [Fact]
        public void Create_document_type_fail_when_name_is_null_or_empty()
        {
            //Arrange
            var name = String.Empty;

            //Act-Assert
            Assert.Throws<ArgumentNullException>(() => new DocumentType(name));
        }

        [Fact]
        public void Change_name_fail_when_name_is_null_or_empty()
        {
            //Arrange
            var name = "documentType";
            var newName = String.Empty;
            var documentType = new DocumentType(name);

            //Act-Assert
            Assert.Throws<ArgumentNullException>(() => documentType.ChangeName(newName));
        }

        [Fact]
        public void Attach_attribute_success()
        {
            //Arrange
            var name = "documentType";
            var documentType = new DocumentType(name);
            var expectedResult = 1;

            //Act
            documentType.AttachAttribute(1);

            //Assert
            Assert.Equal(expectedResult, ((IReadOnlyCollection<BukhatosDocumentTypeAttribute>)documentType.Attributes).Count);
        }

        [Fact]
        public void Attach_attribute_fail_when_attaching_attribute_has_already_attached()
        {
            //Arrange
            var name = "documentType";
            var documentType = new DocumentType(name);
            documentType.AttachAttribute(1);

            //Act-Assert
            Assert.Throws<BukhatosDomainException>(() => documentType.AttachAttribute(1));
        }

        [Fact]
        public void Detach_attribute_success()
        {
            //Arrange
            var name = "documentType";
            var documentType = new DocumentType(name);
            documentType.AttachAttribute(1);
            var expectedResult = 0;

            //Act
            documentType.DetachAttribute(1);

            //Assert
            Assert.Equal(expectedResult, ((IReadOnlyCollection<BukhatosDocumentTypeAttribute>)documentType.Attributes).Count);
        }

        [Fact]
        public void Detach_attribute_fail_when_document_type_doesnt_contain_detaching_attribute()
        {
            //Arrange
            var name = "documentType";
            var documentType = new DocumentType(name);

            //Act-Assert
            Assert.Throws<BukhatosDomainException>(() => documentType.DetachAttribute(1));
        }

        [Fact]
        public void Set_approval_scheme_fail_when_approval_scheme_is_null()
        {
            //Arrange
            var name = "documentType";
            var documentType = new DocumentType(name);
            ApprovalScheme approvalScheme = null;

            //Act-Assert
            Assert.Throws<ArgumentNullException>(() => documentType.SetApprovalScheme(approvalScheme));
        }

        [Fact]
        public void Create_new_document_whithout_approval_scheme_success()
        {
            //Arrange
            var name = "documentType";
            var documentType = new DocumentType(name);
            var createdById = 1;

            //Act
            var document = documentType.CreateNewDocument(createdById);

            //Assert
            Assert.NotNull(document);
        }

        [Fact]
        public void Create_new_document_with_approval_scheme_success()
        {
            //Arrange
            var name = "documentType";
            var documentType = new DocumentType(name);
            var approvalScheme = new ApprovalScheme("approvalScheme");
            documentType.SetApprovalScheme(approvalScheme);
            var createdById = 1;

            //Act
            var document = documentType.CreateNewDocument(createdById);

            //Assert
            Assert.NotNull(document);
        }

    }
}
