﻿using Bukhatos.Domain.Entities.DocumentAttributeAggregate;
using System;
using Xunit;
using BukhatosDocumentAttribute = Bukhatos.Domain.Entities.DocumentAttributeAggregate.Attribute;

namespace Bukhatos.UnitTests.Domain.Entities
{
    public class DocumentAttributeAggregateTest
    {
        [Fact]
        public void Create_attribute_success()
        {
            //Arrange    
            var name = "fakeAttribute";
            var attributeType = AttributeType.StringField;

            //Act 
            var fakeAttribute = new BukhatosDocumentAttribute(name, attributeType);

            //Assert
            Assert.NotNull(fakeAttribute);
        }

        [Fact]
        public void Create_attribute_fail_when_name_is_null_or_empty()
        {
            //Arrange    
            var name = String.Empty;
            var attributeType = AttributeType.StringField;

            //Act - Assert
            Assert.Throws<ArgumentNullException>(() => new BukhatosDocumentAttribute(name, attributeType));
        }

        [Fact]
        public void Create_attribute_fail_when_attributeType_is_null()
        {
            //Arrange    
            var name = "fakeAttribute";
            AttributeType attributeType = null;

            //Act - Assert
            Assert.Throws<ArgumentNullException>(() => new BukhatosDocumentAttribute(name, attributeType));
        }

        [Fact]
        public void Change_attribute_name_fail_when_name_is_null_or_empty()
        {
            //Arrange
            var attributeType = AttributeType.StringField;
            var name = "fakeAttribute";
            var newName = String.Empty;
            var fakeAttribute = new BukhatosDocumentAttribute(name, attributeType);

            //Act - Assert
            Assert.Throws<ArgumentNullException>(() => fakeAttribute.ChangeAttributeName(newName));
        }

        [Fact]
        public void Change_attribute_type_fail_when_type_is_null()
        {
            //Arrange
            var attributeType = AttributeType.StringField;
            AttributeType newAttributeType = null;
            var name = "fakeAttribute";
            var fakeAttribute = new BukhatosDocumentAttribute(name, attributeType);

            //Act - Assert
            Assert.Throws<ArgumentNullException>(() => fakeAttribute.ChangeAttributeType(newAttributeType));
        }
    }
}
