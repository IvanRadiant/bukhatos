﻿using Bukhatos.Domain.Entities.DocumentAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bukhatos.EntityFramework.EntityConfigurations
{
    class StepStatusEntityTypeConfiguration : IEntityTypeConfiguration<StepStatus>
    {
        public void Configure(EntityTypeBuilder<StepStatus> stepStatusConfiguration)
        {
            stepStatusConfiguration.ToTable("StepStatus", BukhatosContext.DEFAULT_SCHEMA);

            stepStatusConfiguration.HasKey(s => s.Id);

            stepStatusConfiguration.Property(s => s.Id).HasDefaultValue(1)
                                                       .ValueGeneratedNever()
                                                       .IsRequired();

            stepStatusConfiguration.Property(s => s.Name)
                                   .HasMaxLength(200)
                                   .IsRequired();

            stepStatusConfiguration.HasMany<StepState>()
                                   .WithOne("_stepStatus")
                                   .HasForeignKey("StepStatusId")
                                   .IsRequired()
                                   .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
