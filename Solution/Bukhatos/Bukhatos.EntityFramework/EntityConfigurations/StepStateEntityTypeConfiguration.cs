﻿using Bukhatos.Domain.Entities.ApprovalSchemeAggregate;
using Bukhatos.Domain.Entities.DocumentAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bukhatos.EntityFramework.EntityConfigurations
{
    class StepStateEntityTypeConfiguration : IEntityTypeConfiguration<StepState>
    {
        public void Configure(EntityTypeBuilder<StepState> stepStateConfiguration)
        {
            stepStateConfiguration.ToTable("StepState", BukhatosContext.DEFAULT_SCHEMA);

            stepStateConfiguration.Property<int>("Id").ValueGeneratedOnAdd();
            stepStateConfiguration.HasKey("Id");

            stepStateConfiguration.OwnsOne(typeof(Comment), "_comment",
                                                commentConf => {
                                                    commentConf.Property<string>("Text").IsRequired();
                                                    commentConf.Property<string>("AttachedFile").IsRequired(false);
                                                }
                                           );

            stepStateConfiguration.Property<int>("StepId").IsRequired();
            stepStateConfiguration.Property<int>("StepStatusId").IsRequired();
            stepStateConfiguration.Property<int?>("ChangedById").IsRequired(false);

            stepStateConfiguration.HasOne<Step>()
                                  .WithMany()
                                  .HasForeignKey("StepId")
                                  .IsRequired()
                                  .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
