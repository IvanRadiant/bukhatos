﻿using Bukhatos.Domain.Entities.ApprovalSchemeAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bukhatos.EntityFramework.EntityConfigurations
{
    class StepEntityTypeConfiguration : IEntityTypeConfiguration<Step>
    {
        public void Configure(EntityTypeBuilder<Step> stepConfiguration)
        {
            stepConfiguration.ToTable("Steps", BukhatosContext.DEFAULT_SCHEMA);

            stepConfiguration.HasKey(s => s.Id);

            stepConfiguration.Property<string>("Name").IsRequired();
            stepConfiguration.Property<int?>("RoleId").IsRequired(false);//Fk's in the future
            stepConfiguration.Property<int?>("NextStepId").IsRequired(false);
            stepConfiguration.Property<int>("ApprovalSchemeId").IsRequired();

            stepConfiguration.HasOne<Step>()
                             .WithOne()
                             .HasForeignKey<Step>("NextStepId")
                             .IsRequired(false)
                             .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
