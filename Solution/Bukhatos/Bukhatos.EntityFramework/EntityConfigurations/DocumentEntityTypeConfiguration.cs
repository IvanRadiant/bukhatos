﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Bukhatos.Domain.Entities.DocumentAggregate;

namespace Bukhatos.EntityFramework.EntityConfigurations
{
    class DocumentEntityTypeConfiguration : IEntityTypeConfiguration<Document>
    {
        public void Configure(EntityTypeBuilder<Document> documentConfiguration)
        {
            documentConfiguration.ToTable("Documents", BukhatosContext.DEFAULT_SCHEMA);

            documentConfiguration.HasKey(d => d.Id);

            documentConfiguration.Ignore(d => d.DomainEvents);

            documentConfiguration.Property(d => d.Id).ValueGeneratedOnAdd();

            documentConfiguration.Property<int>("CreatedById").IsRequired();//FK in the future
            documentConfiguration.Property<DateTime>("CreatedAt").IsRequired();
            documentConfiguration.Property<int>("DocumentTypeId").IsRequired();
            documentConfiguration.Property<int>("DocumentStatusId").IsRequired();

            documentConfiguration.HasOne<ApprovalProcess>("_approvalProcess")
                                 .WithOne()
                                 .HasForeignKey<ApprovalProcess>("DocumentId")
                                 .IsRequired()
                                 .OnDelete(DeleteBehavior.Cascade);

            documentConfiguration.HasMany(d => d.Attributes)
                                 .WithOne()
                                 .HasForeignKey("DocumentId")
                                 .IsRequired()
                                 .OnDelete(DeleteBehavior.Cascade);

            var navigation = documentConfiguration.Metadata.FindNavigation(nameof(Document.Attributes));

            navigation.SetPropertyAccessMode(PropertyAccessMode.Field);
        }
    }
}
