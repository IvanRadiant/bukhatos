﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Bukhatos.Domain.Entities.DocumentAttributeAggregate;
using DocumentTypeAttribute = Bukhatos.Domain.Entities.DocumentTypeAggregate.Attribute;

namespace Bukhatos.EntityFramework.EntityConfigurations
{
    class AttributeEntityTypeConfiguration : IEntityTypeConfiguration<Attribute>
    {
        public void Configure(EntityTypeBuilder<Attribute> attributeConfiguration)
        {
            attributeConfiguration.ToTable("Attribute", BukhatosContext.DEFAULT_SCHEMA);

            attributeConfiguration.HasKey(attr => attr.Id);

            attributeConfiguration.Ignore(attr => attr.DomainEvents);

            attributeConfiguration.Property(attr => attr.Id).ValueGeneratedOnAdd();

            attributeConfiguration.Property<string>("Name").IsRequired();
            attributeConfiguration.Property<int>("TypeId").IsRequired();

            attributeConfiguration.HasMany<DocumentTypeAttribute>()
                                  .WithOne()
                                  .HasForeignKey("AttributeId")
                                  .IsRequired()
                                  .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
