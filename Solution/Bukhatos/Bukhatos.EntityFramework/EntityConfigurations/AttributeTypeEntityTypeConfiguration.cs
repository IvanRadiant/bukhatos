﻿using Bukhatos.Domain.Entities.DocumentAggregate;
using Bukhatos.Domain.Entities.DocumentAttributeAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bukhatos.EntityFramework.EntityConfigurations
{
    class AttributeTypeEntityTypeConfiguration : IEntityTypeConfiguration<AttributeType>
    {
        public void Configure(EntityTypeBuilder<AttributeType> attributeTypeConfiguration)
        {
            attributeTypeConfiguration.ToTable("AttributeType", BukhatosContext.DEFAULT_SCHEMA);

            attributeTypeConfiguration.HasKey(a => a.Id);

            attributeTypeConfiguration.Property(a => a.Id).HasDefaultValue(1)
                                                          .ValueGeneratedNever()
                                                          .IsRequired();

            attributeTypeConfiguration.Property(a => a.Name)
                                      .HasMaxLength(200)
                                      .IsRequired();

            attributeTypeConfiguration.HasMany<Attribute>()
                                      .WithOne("_type")
                                      .HasForeignKey("TypeId")
                                      .IsRequired()
                                      .OnDelete(DeleteBehavior.Cascade);

            attributeTypeConfiguration.HasMany<AttributeValue>()
                                      .WithOne()
                                      .HasForeignKey("AttributeId")
                                      .IsRequired(true)
                                      .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
