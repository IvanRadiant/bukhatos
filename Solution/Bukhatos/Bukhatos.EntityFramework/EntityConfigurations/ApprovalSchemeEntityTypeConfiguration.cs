﻿using Bukhatos.Domain.Entities.ApprovalSchemeAggregate;
using Bukhatos.Domain.Entities.DocumentTypeAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bukhatos.EntityFramework.EntityConfigurations
{
    class ApprovalSchemeEntityTypeConfiguration : IEntityTypeConfiguration<ApprovalScheme>
    {
        public void Configure(EntityTypeBuilder<ApprovalScheme> approvalSchemeConfigurations)
        {
            approvalSchemeConfigurations.ToTable("ApprovalScheme", BukhatosContext.DEFAULT_SCHEMA);

            approvalSchemeConfigurations.HasKey(a => a.Id);
            approvalSchemeConfigurations.Property(a => a.Id).ValueGeneratedOnAdd();

            approvalSchemeConfigurations.Property<string>("Name").IsRequired();

            approvalSchemeConfigurations.HasMany<DocumentType>()
                                        .WithOne("_approvalScheme")
                                        .HasForeignKey("ApprovalSchemeId")
                                        .IsRequired(false)
                                        .OnDelete(DeleteBehavior.Restrict);

            approvalSchemeConfigurations.HasMany(a => a.Steps)
                                        .WithOne()
                                        .HasForeignKey("ApprovalSchemeId")
                                        .OnDelete(DeleteBehavior.Cascade);

            var navigation = approvalSchemeConfigurations.Metadata.FindNavigation(nameof(ApprovalScheme.Steps));

            navigation.SetPropertyAccessMode(PropertyAccessMode.Field);
        }
    }
}
