﻿using Bukhatos.Domain.Entities.DocumentAggregate;
using Bukhatos.Domain.Entities.DocumentAttributeAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bukhatos.EntityFramework.EntityConfigurations
{
    class AttributeValueEntityTypeConfiguration : IEntityTypeConfiguration<AttributeValue>
    {
        public void Configure(EntityTypeBuilder<AttributeValue> attrValueConfiguration)
        {
            attrValueConfiguration.ToTable("AttributeValues", BukhatosContext.DEFAULT_SCHEMA);

            attrValueConfiguration.Property<int>("id").HasColumnName("Id").ValueGeneratedOnAdd();
            attrValueConfiguration.HasKey("id");

            attrValueConfiguration.Property<string>("Value").IsRequired();
            attrValueConfiguration.Property<int>("AttributeId").IsRequired();
            attrValueConfiguration.Property<int>("DocumentId").IsRequired();
            
        }
    }
}
