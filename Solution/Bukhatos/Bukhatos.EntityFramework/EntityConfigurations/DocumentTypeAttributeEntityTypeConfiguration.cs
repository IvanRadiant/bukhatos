﻿using Bukhatos.Domain.Entities.DocumentTypeAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bukhatos.EntityFramework.EntityConfigurations
{
    class DocumentTypeAttributeEntityTypeConfiguration : IEntityTypeConfiguration<Attribute>
    {
        public void Configure(EntityTypeBuilder<Attribute> docTypeAttributeConfiguration)
        {
            docTypeAttributeConfiguration.ToTable("DocumentTypeAttribute", BukhatosContext.DEFAULT_SCHEMA);

            docTypeAttributeConfiguration.Property<int>("Id").ValueGeneratedOnAdd();
            docTypeAttributeConfiguration.HasKey("Id");
        
            docTypeAttributeConfiguration.Property<int>("AttributeId").IsRequired();
            docTypeAttributeConfiguration.Property<int>("DocumentTypeId").IsRequired();
        }
    }
}
