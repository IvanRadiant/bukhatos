﻿using Bukhatos.Domain.Entities.DocumentAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bukhatos.EntityFramework.EntityConfigurations
{
    class DocumentStatusEntityTypeConfiguration : IEntityTypeConfiguration<DocumentStatus>
    {
        public void Configure(EntityTypeBuilder<DocumentStatus> processStatusConfiguration)
        {
            processStatusConfiguration.ToTable("DocumentStatus", BukhatosContext.DEFAULT_SCHEMA);

            processStatusConfiguration.HasKey(p => p.Id);

            processStatusConfiguration.Property(p => p.Id).HasDefaultValue(1)
                                                          .ValueGeneratedNever()
                                                          .IsRequired();

            processStatusConfiguration.Property(p => p.Name)
                                      .HasMaxLength(200)
                                      .IsRequired();

            processStatusConfiguration.HasMany<Document>()
                                      .WithOne("_documentStatus")
                                      .HasForeignKey("DocumentStatusId")
                                      .IsRequired()
                                      .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
