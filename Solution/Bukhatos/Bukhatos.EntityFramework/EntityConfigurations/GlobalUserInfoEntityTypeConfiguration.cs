﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Bukhatos.Domain.Entities.UserAggregate;

namespace Bukhatos.EntityFramework.EntityConfigurations
{
    class GlobalUserInfoEntityTypeConfiguration : IEntityTypeConfiguration<GlobalUserInfo>
    {
        public void Configure(EntityTypeBuilder<GlobalUserInfo> globalUserInfoConfiguration)
        {
            globalUserInfoConfiguration.ToTable("GlobalUserInfo", BukhatosContext.DEFAULT_SCHEMA);

            globalUserInfoConfiguration.Property<int>("id").HasColumnName("Id").ValueGeneratedOnAdd();
            globalUserInfoConfiguration.HasKey("id");

            globalUserInfoConfiguration.Property<string>("DAS").IsRequired();
            globalUserInfoConfiguration.Property<string>("PersonalNum").IsRequired();
            globalUserInfoConfiguration.Property<string>("Department").IsRequired();
            globalUserInfoConfiguration.Property<string>("TabelNum").IsRequired();
            globalUserInfoConfiguration.Property<string>("MB3").IsRequired();

        }
    }
}
