﻿using Bukhatos.Domain.Entities.DocumentAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bukhatos.EntityFramework.EntityConfigurations
{
    class ApprovalProcessEntityTypeConfiguration : IEntityTypeConfiguration<ApprovalProcess>
    {
        public void Configure(EntityTypeBuilder<ApprovalProcess> approvalProcessConfiguration)
        {
            approvalProcessConfiguration.ToTable("ApprovalProcess", BukhatosContext.DEFAULT_SCHEMA);

            approvalProcessConfiguration.HasKey(a => a.Id);

            approvalProcessConfiguration.Property(a => a.Id).ValueGeneratedOnAdd();

            approvalProcessConfiguration.Property<int>("DocumentId").IsRequired();

            approvalProcessConfiguration.HasMany(a => a.Steps)
                                        .WithOne()
                                        .HasForeignKey("DocumentTypeId")
                                        .IsRequired()
                                        .OnDelete(DeleteBehavior.Cascade);

            var navigation = approvalProcessConfiguration.Metadata.FindNavigation(nameof(ApprovalProcess.Steps));

            navigation.SetPropertyAccessMode(PropertyAccessMode.Field);

        }
    }
}
