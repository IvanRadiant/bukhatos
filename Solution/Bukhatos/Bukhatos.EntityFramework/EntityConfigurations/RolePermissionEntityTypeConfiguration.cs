﻿using Bukhatos.Domain.Entities.RoleAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bukhatos.EntityFramework.EntityConfigurations
{
    class RolePermissionEntityTypeConfiguration : IEntityTypeConfiguration<Permission>
    {
        public void Configure(EntityTypeBuilder<Permission> rolePermissionConfiguration)
        {
            rolePermissionConfiguration.ToTable("RolePermission", BukhatosContext.DEFAULT_SCHEMA);

            rolePermissionConfiguration.Property<int>("Id").ValueGeneratedOnAdd();
            rolePermissionConfiguration.HasKey("Id");

            rolePermissionConfiguration.Property<int>("RoleId").IsRequired();
            rolePermissionConfiguration.Property<int>("PermissionId").IsRequired();
        }
    }
}
