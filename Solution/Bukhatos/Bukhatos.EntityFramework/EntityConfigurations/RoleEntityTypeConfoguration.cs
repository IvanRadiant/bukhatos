﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Bukhatos.Domain.Entities.RoleAggregate;
using UserRole = Bukhatos.Domain.Entities.UserAggregate.Role;

namespace Bukhatos.EntityFramework.EntityConfigurations
{
    class RoleEntityTypeConfoguration : IEntityTypeConfiguration<Role>
    {
        public void Configure(EntityTypeBuilder<Role> roleConfiguration)
        {
            roleConfiguration.ToTable("Role", BukhatosContext.DEFAULT_SCHEMA);

            roleConfiguration.HasKey(a => a.Id);
            roleConfiguration.Property(a => a.Id).ValueGeneratedOnAdd();

            roleConfiguration.Ignore(d => d.DomainEvents);

            roleConfiguration.Property<string>("Name").IsRequired();

            roleConfiguration.HasMany(a => a.Permissions)
                             .WithOne()
                             .HasForeignKey("RoleId")
                             .IsRequired()
                             .OnDelete(DeleteBehavior.Cascade);
            
            roleConfiguration.HasMany<UserRole>()
                             .WithOne()
                             .HasForeignKey("RoleId")
                             .IsRequired()
                             .OnDelete(DeleteBehavior.Cascade);
                             
           var navigation = roleConfiguration.Metadata.FindNavigation(nameof(Role.Permissions));
           navigation.SetPropertyAccessMode(PropertyAccessMode.Field);
        }
    }
}
