﻿using Bukhatos.Domain.Entities.ApprovalSchemeAggregate;
using Bukhatos.Domain.Entities.DocumentAggregate;
using Bukhatos.Domain.Entities.DocumentTypeAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bukhatos.EntityFramework.EntityConfigurations
{
    class DocumentTypeEntityTypeConfiguration : IEntityTypeConfiguration<DocumentType>
    {
        public void Configure(EntityTypeBuilder<DocumentType> documentTypeConfiguration)
        {
            documentTypeConfiguration.ToTable("DocumentType", BukhatosContext.DEFAULT_SCHEMA);

            documentTypeConfiguration.Ignore(d => d.DomainEvents);

            documentTypeConfiguration.HasKey(d => d.Id);

            documentTypeConfiguration.Property(d => d.Id).ValueGeneratedOnAdd();

            documentTypeConfiguration.Property<string>("Name").IsRequired();
            documentTypeConfiguration.Property<int?>("ApprovalSchemeId").IsRequired(false);

            documentTypeConfiguration.HasMany<Document>()
                                     .WithOne()
                                     .HasForeignKey("DocumentTypeId")
                                     .IsRequired()
                                     .OnDelete(DeleteBehavior.Cascade);

            documentTypeConfiguration.HasMany(d => d.Attributes)
                                     .WithOne()
                                     .HasForeignKey("DocumentTypeId")
                                     .OnDelete(DeleteBehavior.Cascade);

            var navigation = documentTypeConfiguration.Metadata.FindNavigation(nameof(DocumentType.Attributes));

            navigation.SetPropertyAccessMode(PropertyAccessMode.Field);
        }
    }
}
