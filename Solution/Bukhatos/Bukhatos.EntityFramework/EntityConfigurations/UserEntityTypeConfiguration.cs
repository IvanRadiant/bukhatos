﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Bukhatos.Domain.Entities.UserAggregate;
using Bukhatos.Domain.Entities.DocumentAggregate;

namespace Bukhatos.EntityFramework.EntityConfigurations
{
    class UserEntityTypeConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> userConfiguration)
        {
            userConfiguration.ToTable("User", BukhatosContext.DEFAULT_SCHEMA);

            userConfiguration.HasKey(a => a.Id);

            userConfiguration.Property(a => a.Id).ValueGeneratedOnAdd();

            userConfiguration.Property<string>("UserName").IsRequired();

            userConfiguration.OwnsOne(typeof(GlobalUserInfo), "_globalUserInfo",
                                      userInfo => {
                                          userInfo.Property<string>("DAS").IsRequired();
                                          userInfo.Property<string>("PersonalNum").IsRequired();
                                          userInfo.Property<string>("Department").IsRequired();
                                          userInfo.Property<string>("TabelNum").IsRequired();
                                          userInfo.Property<string>("MB3").IsRequired();
                                      });

            userConfiguration.HasMany(d => d.Roles)
                             .WithOne()
                             .HasForeignKey("UserId")
                             .OnDelete(DeleteBehavior.Restrict);

            userConfiguration.HasMany<Document>()
                             .WithOne()
                             .HasForeignKey("CreatedById")
                             .OnDelete(DeleteBehavior.Restrict);

            var navigation = userConfiguration.Metadata.FindNavigation(nameof(User.Roles));
            navigation.SetPropertyAccessMode(PropertyAccessMode.Field);
                        
                      


        }
    }
}
