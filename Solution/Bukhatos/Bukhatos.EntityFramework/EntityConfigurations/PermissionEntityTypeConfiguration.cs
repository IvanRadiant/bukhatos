﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Bukhatos.Domain.Entities.PermissionAggregate;
using RolePermission = Bukhatos.Domain.Entities.RoleAggregate.Permission;

namespace Bukhatos.EntityFramework.EntityConfigurations
{
    class PermissionEntityTypeConfiguration : IEntityTypeConfiguration<Permission>
    {
        public void Configure(EntityTypeBuilder<Permission> permissionConfiguration)
        {
            permissionConfiguration.ToTable("Permission", BukhatosContext.DEFAULT_SCHEMA);

            permissionConfiguration.HasKey(a => a.Id);

            permissionConfiguration.Property(a => a.Id).ValueGeneratedOnAdd();

            permissionConfiguration.Property<string>("Name").IsRequired();

            permissionConfiguration.HasMany<RolePermission>()
                                   .WithOne()
                                   .HasForeignKey("PermissionId")
                                   .IsRequired()
                                   .OnDelete(DeleteBehavior.Cascade);

        }
    }
}
