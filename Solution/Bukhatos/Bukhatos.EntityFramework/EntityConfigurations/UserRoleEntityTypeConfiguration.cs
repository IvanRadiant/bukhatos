﻿using Bukhatos.Domain.Entities.UserAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bukhatos.EntityFramework.EntityConfigurations
{
    class UserRoleEntityTypeConfiguration : IEntityTypeConfiguration<Role>
    {
        public void Configure(EntityTypeBuilder<Role> userRoleConfiguration)
        {
            userRoleConfiguration.ToTable("UserRole", BukhatosContext.DEFAULT_SCHEMA);

            userRoleConfiguration.Property<int>("Id").ValueGeneratedOnAdd();
            userRoleConfiguration.HasKey("Id");

            userRoleConfiguration.Property<int>("UserId").IsRequired();
            userRoleConfiguration.Property<int>("RoleId").IsRequired();
        }
    }
}
