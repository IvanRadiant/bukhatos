﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Bukhatos.EntityFramework.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "bukhatos");

            migrationBuilder.CreateTable(
                name: "ApprovalScheme",
                schema: "bukhatos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApprovalScheme", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AttributeType",
                schema: "bukhatos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false, defaultValue: 1),
                    Name = table.Column<string>(maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AttributeType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DocumentStatus",
                schema: "bukhatos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false, defaultValue: 1),
                    Name = table.Column<string>(maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocumentStatus", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Permission",
                schema: "bukhatos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Permission", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Role",
                schema: "bukhatos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Role", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "StepStatus",
                schema: "bukhatos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false, defaultValue: 1),
                    Name = table.Column<string>(maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StepStatus", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "User",
                schema: "bukhatos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserName = table.Column<string>(nullable: false),
                    _globalUserInfo_DAS = table.Column<string>(nullable: false),
                    _globalUserInfo_Department = table.Column<string>(nullable: false),
                    _globalUserInfo_MB3 = table.Column<string>(nullable: false),
                    _globalUserInfo_PersonalNum = table.Column<string>(nullable: false),
                    _globalUserInfo_TabelNum = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DocumentType",
                schema: "bukhatos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ApprovalSchemeId = table.Column<int>(nullable: true),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocumentType", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DocumentType_ApprovalScheme_ApprovalSchemeId",
                        column: x => x.ApprovalSchemeId,
                        principalSchema: "bukhatos",
                        principalTable: "ApprovalScheme",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Steps",
                schema: "bukhatos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ApprovalSchemeId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    NextStepId = table.Column<int>(nullable: true),
                    RoleId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Steps", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Steps_ApprovalScheme_ApprovalSchemeId",
                        column: x => x.ApprovalSchemeId,
                        principalSchema: "bukhatos",
                        principalTable: "ApprovalScheme",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Steps_Steps_NextStepId",
                        column: x => x.NextStepId,
                        principalSchema: "bukhatos",
                        principalTable: "Steps",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Attribute",
                schema: "bukhatos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    TypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Attribute", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Attribute_AttributeType_TypeId",
                        column: x => x.TypeId,
                        principalSchema: "bukhatos",
                        principalTable: "AttributeType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RolePermission",
                schema: "bukhatos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PermissionId = table.Column<int>(nullable: false),
                    RoleId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RolePermission", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RolePermission_Permission_PermissionId",
                        column: x => x.PermissionId,
                        principalSchema: "bukhatos",
                        principalTable: "Permission",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RolePermission_Role_RoleId",
                        column: x => x.RoleId,
                        principalSchema: "bukhatos",
                        principalTable: "Role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserRole",
                schema: "bukhatos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RoleId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRole", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserRole_Role_RoleId",
                        column: x => x.RoleId,
                        principalSchema: "bukhatos",
                        principalTable: "Role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserRole_User_UserId",
                        column: x => x.UserId,
                        principalSchema: "bukhatos",
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Documents",
                schema: "bukhatos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    CreatedById = table.Column<int>(nullable: false),
                    DocumentStatusId = table.Column<int>(nullable: false),
                    DocumentTypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Documents", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Documents_User_CreatedById",
                        column: x => x.CreatedById,
                        principalSchema: "bukhatos",
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Documents_DocumentStatus_DocumentStatusId",
                        column: x => x.DocumentStatusId,
                        principalSchema: "bukhatos",
                        principalTable: "DocumentStatus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Documents_DocumentType_DocumentTypeId",
                        column: x => x.DocumentTypeId,
                        principalSchema: "bukhatos",
                        principalTable: "DocumentType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DocumentTypeAttribute",
                schema: "bukhatos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AttributeId = table.Column<int>(nullable: false),
                    DocumentTypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocumentTypeAttribute", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DocumentTypeAttribute_Attribute_AttributeId",
                        column: x => x.AttributeId,
                        principalSchema: "bukhatos",
                        principalTable: "Attribute",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DocumentTypeAttribute_DocumentType_DocumentTypeId",
                        column: x => x.DocumentTypeId,
                        principalSchema: "bukhatos",
                        principalTable: "DocumentType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ApprovalProcess",
                schema: "bukhatos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DocumentId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApprovalProcess", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ApprovalProcess_Documents_DocumentId",
                        column: x => x.DocumentId,
                        principalSchema: "bukhatos",
                        principalTable: "Documents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AttributeValues",
                schema: "bukhatos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AttributeId = table.Column<int>(nullable: false),
                    DocumentId = table.Column<int>(nullable: false),
                    Value = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AttributeValues", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AttributeValues_AttributeType_AttributeId",
                        column: x => x.AttributeId,
                        principalSchema: "bukhatos",
                        principalTable: "AttributeType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AttributeValues_Documents_DocumentId",
                        column: x => x.DocumentId,
                        principalSchema: "bukhatos",
                        principalTable: "Documents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "StepState",
                schema: "bukhatos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ChangedById = table.Column<int>(nullable: true),
                    DocumentTypeId = table.Column<int>(nullable: false),
                    StepId = table.Column<int>(nullable: false),
                    StepStatusId = table.Column<int>(nullable: false),
                    _comment_AttachedFile = table.Column<string>(nullable: true),
                    _comment_Text = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StepState", x => x.Id);
                    table.ForeignKey(
                        name: "FK_StepState_ApprovalProcess_DocumentTypeId",
                        column: x => x.DocumentTypeId,
                        principalSchema: "bukhatos",
                        principalTable: "ApprovalProcess",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StepState_Steps_StepId",
                        column: x => x.StepId,
                        principalSchema: "bukhatos",
                        principalTable: "Steps",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StepState_StepStatus_StepStatusId",
                        column: x => x.StepStatusId,
                        principalSchema: "bukhatos",
                        principalTable: "StepStatus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ApprovalProcess_DocumentId",
                schema: "bukhatos",
                table: "ApprovalProcess",
                column: "DocumentId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Attribute_TypeId",
                schema: "bukhatos",
                table: "Attribute",
                column: "TypeId");

            migrationBuilder.CreateIndex(
                name: "IX_AttributeValues_AttributeId",
                schema: "bukhatos",
                table: "AttributeValues",
                column: "AttributeId");

            migrationBuilder.CreateIndex(
                name: "IX_AttributeValues_DocumentId",
                schema: "bukhatos",
                table: "AttributeValues",
                column: "DocumentId");

            migrationBuilder.CreateIndex(
                name: "IX_Documents_CreatedById",
                schema: "bukhatos",
                table: "Documents",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_Documents_DocumentStatusId",
                schema: "bukhatos",
                table: "Documents",
                column: "DocumentStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Documents_DocumentTypeId",
                schema: "bukhatos",
                table: "Documents",
                column: "DocumentTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentType_ApprovalSchemeId",
                schema: "bukhatos",
                table: "DocumentType",
                column: "ApprovalSchemeId");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentTypeAttribute_AttributeId",
                schema: "bukhatos",
                table: "DocumentTypeAttribute",
                column: "AttributeId");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentTypeAttribute_DocumentTypeId",
                schema: "bukhatos",
                table: "DocumentTypeAttribute",
                column: "DocumentTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_RolePermission_PermissionId",
                schema: "bukhatos",
                table: "RolePermission",
                column: "PermissionId");

            migrationBuilder.CreateIndex(
                name: "IX_RolePermission_RoleId",
                schema: "bukhatos",
                table: "RolePermission",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_Steps_ApprovalSchemeId",
                schema: "bukhatos",
                table: "Steps",
                column: "ApprovalSchemeId");

            migrationBuilder.CreateIndex(
                name: "IX_Steps_NextStepId",
                schema: "bukhatos",
                table: "Steps",
                column: "NextStepId",
                unique: true,
                filter: "[NextStepId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_StepState_DocumentTypeId",
                schema: "bukhatos",
                table: "StepState",
                column: "DocumentTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_StepState_StepId",
                schema: "bukhatos",
                table: "StepState",
                column: "StepId");

            migrationBuilder.CreateIndex(
                name: "IX_StepState_StepStatusId",
                schema: "bukhatos",
                table: "StepState",
                column: "StepStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_UserRole_RoleId",
                schema: "bukhatos",
                table: "UserRole",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_UserRole_UserId",
                schema: "bukhatos",
                table: "UserRole",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AttributeValues",
                schema: "bukhatos");

            migrationBuilder.DropTable(
                name: "DocumentTypeAttribute",
                schema: "bukhatos");

            migrationBuilder.DropTable(
                name: "RolePermission",
                schema: "bukhatos");

            migrationBuilder.DropTable(
                name: "StepState",
                schema: "bukhatos");

            migrationBuilder.DropTable(
                name: "UserRole",
                schema: "bukhatos");

            migrationBuilder.DropTable(
                name: "Attribute",
                schema: "bukhatos");

            migrationBuilder.DropTable(
                name: "Permission",
                schema: "bukhatos");

            migrationBuilder.DropTable(
                name: "ApprovalProcess",
                schema: "bukhatos");

            migrationBuilder.DropTable(
                name: "Steps",
                schema: "bukhatos");

            migrationBuilder.DropTable(
                name: "StepStatus",
                schema: "bukhatos");

            migrationBuilder.DropTable(
                name: "Role",
                schema: "bukhatos");

            migrationBuilder.DropTable(
                name: "AttributeType",
                schema: "bukhatos");

            migrationBuilder.DropTable(
                name: "Documents",
                schema: "bukhatos");

            migrationBuilder.DropTable(
                name: "User",
                schema: "bukhatos");

            migrationBuilder.DropTable(
                name: "DocumentStatus",
                schema: "bukhatos");

            migrationBuilder.DropTable(
                name: "DocumentType",
                schema: "bukhatos");

            migrationBuilder.DropTable(
                name: "ApprovalScheme",
                schema: "bukhatos");
        }
    }
}
