﻿using Bukhatos.Domain.Entities.ApprovalSchemeAggregate;
using Bukhatos.Domain.Entities.DocumentAggregate;
using Bukhatos.Domain.Entities.DocumentAttributeAggregate;
using Bukhatos.Domain.Entities.DocumentTypeAggregate;
using Bukhatos.Domain.Entities.PermissionAggregate;
using Bukhatos.Domain.Entities.RoleAggregate;
using Bukhatos.Domain.Entities.UserAggregate;
using Bukhatos.Domain.SeedWork;
using Bukhatos.EntityFramework.EntityConfigurations;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using BukhatosDomainAttribute = Bukhatos.Domain.Entities.DocumentAttributeAggregate.Attribute;
using BukhatosDomainDocumentTypeAttribute = Bukhatos.Domain.Entities.DocumentTypeAggregate.Attribute;
using Role = Bukhatos.Domain.Entities.RoleAggregate.Role;
using Permission = Bukhatos.Domain.Entities.PermissionAggregate.Permission;
using User = Bukhatos.Domain.Entities.UserAggregate.User;

namespace Bukhatos.EntityFramework
{
    public class BukhatosContext : DbContext, IUnitOfWork
    {
        public const string DEFAULT_SCHEMA = "bukhatos";

        public BukhatosContext() : base()
        {

        }

        public DbSet<Document> Documents { get; set; }
        public DbSet<AttributeValue> AttributeValues { get; set; }
        public DbSet<BukhatosDomainAttribute> Attributes { get; set; }
        public DbSet<AttributeType> AttributeTypes { get; set; }
        public DbSet<ApprovalProcess> ApprovalProcesses { get; set; }
        public DbSet<StepState> StepStates { get; set; }
        public DbSet<StepStatus> StepStatuses { get; set; }
        public DbSet<Step> Steps { get; set; }
        public DbSet<ApprovalScheme> ApprovalSchemes { get; set; }
        public DbSet<DocumentType> DocumentTypes { get; set; }
        public DbSet<BukhatosDomainDocumentTypeAttribute> DocumentTypeAttributes { get; set; }

        public DbSet<Role> Roles { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<Bukhatos.Domain.Entities.UserAggregate.Role> UserRole { get; set; }
        public DbSet<Bukhatos.Domain.Entities.RoleAggregate.Permission> RolePermission { get; set; }


    private readonly IMediator _mediator;
        private IDbContextTransaction _currentTransaction;

        private BukhatosContext(DbContextOptions<BukhatosContext> options) : base(options) { }

        public IDbContextTransaction GetCurrentTransaction => _currentTransaction;

        public BukhatosContext(DbContextOptions<BukhatosContext> options, IMediator mediator) : base(options)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));


            System.Diagnostics.Debug.WriteLine("BukhatosContext::ctor ->" + this.GetHashCode());
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new PermissionEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new RoleEntityTypeConfoguration());
            modelBuilder.ApplyConfiguration(new UserEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new RolePermissionEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new UserRoleEntityTypeConfiguration());

            modelBuilder.ApplyConfiguration(new ApprovalProcessEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new AttributeEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new AttributeTypeEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new AttributeValueEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new DocumentEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new DocumentStatusEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new StepEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new StepStateEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new StepStatusEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ApprovalSchemeEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new DocumentTypeAttributeEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new DocumentTypeEntityTypeConfiguration());
        }

        public async Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            // Dispatch Domain Events collection. 
            // Choices:
            // A) Right BEFORE committing data (EF SaveChanges) into the DB will make a single transaction including  
            // side effects from the domain event handlers which are using the same DbContext with "InstancePerLifetimeScope" or "scoped" lifetime
            // B) Right AFTER committing data (EF SaveChanges) into the DB will make multiple transactions. 
            // You will need to handle eventual consistency and compensatory actions in case of failures in any of the Handlers. 
            await _mediator.DispatchDomainEventsAsync(this);

            // After executing this line all the changes (from the Command Handler and Domain Event Handlers) 
            // performed through the DbContext will be committed
            var result = await base.SaveChangesAsync();

            return true;
        }

        public async Task BeginTransactionAsync()
        {
            _currentTransaction = _currentTransaction ?? await Database.BeginTransactionAsync(IsolationLevel.ReadCommitted);
        }

        public async Task CommitTransactionAsync()
        {
            try
            {
                await SaveChangesAsync();
                _currentTransaction?.Commit();
            }
            catch
            {
                RollbackTransaction();
                throw;
            }
            finally
            {
                if (_currentTransaction != null)
                {
                    _currentTransaction.Dispose();
                    _currentTransaction = null;
                }
            }
        }

        public void RollbackTransaction()
        {
            try
            {
                _currentTransaction?.Rollback();
            }
            finally
            {
                if (_currentTransaction != null)
                {
                    _currentTransaction.Dispose();
                    _currentTransaction = null;
                }
            }
        }
    }

    public class BukhatosContextDesignFactory : IDesignTimeDbContextFactory<BukhatosContext>
    {
        public BukhatosContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<BukhatosContext>()
                .UseSqlServer("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=BukhatosTestDB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");

            return new BukhatosContext(optionsBuilder.Options, new NoMediator());
        }

        class NoMediator : IMediator
        {
            public Task Publish<TNotification>(TNotification notification, CancellationToken cancellationToken = default(CancellationToken)) where TNotification : INotification
            {
                return Task.CompletedTask;
            }

            public Task Publish(object notification, CancellationToken cancellationToken = default(CancellationToken))
            {
                throw new NotImplementedException();
            }

            public Task<TResponse> Send<TResponse>(IRequest<TResponse> request, CancellationToken cancellationToken = default(CancellationToken))
            {
                return Task.FromResult<TResponse>(default(TResponse));
            }

            public Task Send(IRequest request, CancellationToken cancellationToken = default(CancellationToken))
            {
                return Task.CompletedTask;
            }
        }
    }
}
