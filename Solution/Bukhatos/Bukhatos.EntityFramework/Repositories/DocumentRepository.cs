﻿using System;
using System.Threading.Tasks;
using Bukhatos.Domain.Entities.DocumentAggregate;
using Bukhatos.Domain.SeedWork;

namespace Bukhatos.EntityFramework.Repositories
{
    public class DocumentRepository : IDocumentRepository
    {
        private readonly BukhatosContext _context;

        public IUnitOfWork UnitOfWork
        {
            get
            {
                return _context;
            }
        }

        public DocumentRepository(BukhatosContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public Document Add(Document document)
        {
            return _context.Documents.Add(document).Entity;
        }

        public async Task<Document> GetAsync(int documentId)
        {
            var document = await _context.Documents.FindAsync(documentId);
            
            if(document != null)
            {
                await _context.Entry(document).Collection(d => d.Attributes).LoadAsync();
                await _context.Entry(document).Reference("_approvalProcess").LoadAsync();
            }

            return document;
        }

        public void Update(Document document)
        {
            _context.Documents.Update(document);
        }
    }
}
