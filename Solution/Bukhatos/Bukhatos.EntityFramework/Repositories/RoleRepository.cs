﻿using System;
using System.Threading.Tasks;
using Bukhatos.Domain.Entities.RoleAggregate;
using Bukhatos.Domain.SeedWork;

namespace Bukhatos.EntityFramework.Repositories
{
    public class RoleRepository : IRoleRepository
    {
        private readonly BukhatosContext _context;

        public IUnitOfWork UnitOfWork
        {
            get
            {
                return _context;
            }
        }

        public RoleRepository(BukhatosContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public Role Add(Role role)
        {
            return _context.Roles.Add(role).Entity;
        }

        public void Remove(Role role)
        {
            _context.Roles.Remove(role);
        }

        public void Update(Role role)
        {
            _context.Roles.Update(role);
        }

        public async Task<Role> GetAsync(int roleId)
        {
            var role = await _context.Roles.FindAsync(roleId);

            if (role != null)
            {
                await _context.Entry(role).Collection(a => a.Permissions).LoadAsync();
            }

            return role;
        }
    }
}
