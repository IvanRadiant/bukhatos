﻿using System;
using System.Threading.Tasks;
using Bukhatos.Domain.Entities.DocumentTypeAggregate;
using Bukhatos.Domain.SeedWork;

namespace Bukhatos.EntityFramework.Repositories
{
    public class DocumentTypeRepository : IDocumentTypeRepository
    {
        private readonly BukhatosContext _context;

        public IUnitOfWork UnitOfWork
        {
            get
            {
                return _context;
            }
        }

        public DocumentTypeRepository(BukhatosContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public DocumentType Add(DocumentType documentType)
        {
            return _context.DocumentTypes.Add(documentType).Entity;
        }

        public async Task<DocumentType> GetAsync(int documentTypeId)
        {
            var documentType = await _context.DocumentTypes.FindAsync(documentTypeId);

            if(documentType != null)
            {
                await _context.Entry(documentType).Collection(d => d.Attributes).LoadAsync();
                await _context.Entry(documentType).Reference("_approvalScheme").LoadAsync();
            }

            return documentType;
        }

        public void Update(DocumentType documentType)
        {
            _context.DocumentTypes.Update(documentType);
        }
    }
}
