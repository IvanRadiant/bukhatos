﻿using System;
using System.Threading.Tasks;
using Bukhatos.Domain.Entities.DocumentAttributeAggregate;
using Bukhatos.Domain.SeedWork;
using DocumentAttribute = Bukhatos.Domain.Entities.DocumentAttributeAggregate.Attribute;

namespace Bukhatos.EntityFramework.Repositories
{
    public class AttributeRepository : IAttributeRepository
    {
        private readonly BukhatosContext _context;

        public IUnitOfWork UnitOfWork
        {
            get
            {
                return _context;
            }
        }

        public AttributeRepository(BukhatosContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public void Update(DocumentAttribute attribute)
        {
            _context.Attributes.Update(attribute);
        }

        public void Remove(DocumentAttribute attribute)
        {
            _context.Attributes.Remove(attribute);
        }

        public DocumentAttribute Add(DocumentAttribute attribute)
        {
            return _context.Attributes.Add(attribute).Entity;
        }

        public async Task<DocumentAttribute> GetAsync(int attributeId)
        {
            var attribute = await _context.Attributes.FindAsync(attributeId);

            if(attribute != null)
            {
                await _context.Entry(attribute).Reference("_type").LoadAsync();
            }

            return attribute;
        }
    }
}
