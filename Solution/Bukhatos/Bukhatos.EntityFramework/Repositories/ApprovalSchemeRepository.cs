﻿using System;
using System.Threading.Tasks;
using Bukhatos.Domain.Entities.ApprovalSchemeAggregate;
using Bukhatos.Domain.SeedWork;

namespace Bukhatos.EntityFramework.Repositories
{
    public class ApprovalSchemeRepository : IApprovalSchemeRepository
    {
        private readonly BukhatosContext _context;

        public IUnitOfWork UnitOfWork
        {
            get
            {
                return _context;
            }
        }

        public ApprovalSchemeRepository(BukhatosContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public ApprovalScheme Add(ApprovalScheme approvalScheme)
        {
            return _context.ApprovalSchemes.Add(approvalScheme).Entity;
        }

        public void Remove(ApprovalScheme approvalScheme)
        {
            _context.ApprovalSchemes.Remove(approvalScheme);
        }

        public async Task<ApprovalScheme> GetAsync(int approvalSchemeId)
        {
            var approvalScheme = await _context.ApprovalSchemes.FindAsync(approvalSchemeId);

            if (approvalScheme != null)
            {
                await _context.Entry(approvalScheme).Collection(a => a.Steps).LoadAsync();
            }

            return approvalScheme;
        }

        public void Update(ApprovalScheme approvalScheme)
        {
            _context.ApprovalSchemes.Update(approvalScheme);
        }
    }
}
