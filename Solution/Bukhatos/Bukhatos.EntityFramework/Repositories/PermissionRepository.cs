﻿using System;
using System.Threading.Tasks;
using Bukhatos.Domain.Entities.PermissionAggregate;
using Bukhatos.Domain.SeedWork;

namespace Bukhatos.EntityFramework.Repositories
{
    public class PermissionRepository : IPermissionRepository
    {
        private readonly BukhatosContext _context;

        public IUnitOfWork UnitOfWork
        {
            get
            {
                return _context;
            }
        }

        public PermissionRepository(BukhatosContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public Permission Add(Permission permission)
        {
            return _context.Permissions.Add(permission).Entity;
        }

        public void Remove(Permission permission)
        {
            _context.Permissions.Remove(permission);
        }

        public void Update(Permission permision)
        {
            _context.Permissions.Update(permision);
        }

        public async Task<Permission> GetAsync(int permissionId)
        {
            var permission = await _context.Permissions.FindAsync(permissionId);

            return permission;
        }
    }
}
