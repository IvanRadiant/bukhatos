﻿using Bukhatos.Domain.Entities.UserAggregate;
using Bukhatos.Domain.Events.Document;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Bukhatos.Application.DomainEventHandlers
{
    public class DocumentStepApprovedDomainEventHandler
                            : INotificationHandler<DocumentStepApprovedDomainEvent>
    {
        private readonly ILoggerFactory _logger;
        private readonly IUserRepository _userRepository;

        public DocumentStepApprovedDomainEventHandler(
                                ILoggerFactory logger,
                                IUserRepository userRepository
                            )
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _userRepository = _userRepository ?? throw new ArgumentNullException(nameof(userRepository));
        }

        public async Task Handle(DocumentStepApprovedDomainEvent documentStepApprovedDomainEventHandler, CancellationToken cancellationToken)
        {
            var user = await _userRepository.GetAsync(documentStepApprovedDomainEventHandler.ApprovedById);

            _logger.CreateLogger<DocumentSentForApprovalDomainEventHandler>()
                   .LogTrace("Approval step of a document with Id: {0} has been approved by user {1} with Id: {2}",
                              documentStepApprovedDomainEventHandler.DocId, user.GetFIO(), user.Id);
        }
    }
}
