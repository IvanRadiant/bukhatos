﻿using Bukhatos.Domain.Entities.DocumentAttributeAggregate;
using Bukhatos.Domain.Entities.UserAggregate;
using Bukhatos.Domain.Events.Document;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Bukhatos.Application.DomainEventHandlers
{
    public class AttributeValueChangedDomainEventHandler
                                    : INotificationHandler<AttributeValueChangedDomainEvent>
    {
        private readonly ILoggerFactory _logger;
        private readonly IAttributeRepository _attributeRepository;

        public AttributeValueChangedDomainEventHandler(
                                ILoggerFactory logger,
                                IAttributeRepository attributeRepository
                            )
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _attributeRepository = attributeRepository ?? throw new ArgumentNullException(nameof(attributeRepository));
        }

        public async Task Handle(AttributeValueChangedDomainEvent attributeValueChangedDomainEvent, CancellationToken cancellationToken)
        {
            var attribute = await _attributeRepository.GetAsync(attributeValueChangedDomainEvent.AttributeId);

            _logger.CreateLogger<DocumentSentForApprovalDomainEventHandler>()
                   .LogTrace("Value of the attribute: '{0}' of the document with Id: {1} has been changed from '{2}' to '{3}'",
                              attribute.GetName(),
                              attributeValueChangedDomainEvent.DocId,
                              attributeValueChangedDomainEvent.OldValue,
                              attributeValueChangedDomainEvent.Value);
        }
    }
}
