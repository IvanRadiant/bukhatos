﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Bukhatos.Domain.Entities.UserAggregate;
using Bukhatos.Domain.Events.Document;
using MediatR;
using Microsoft.Extensions.Logging;

namespace Bukhatos.Application.DomainEventHandlers
{
    public class DocumentSentForApprovalDomainEventHandler
                    : INotificationHandler<DocumentSentForApprovalDomainEvent>
    {
        private readonly ILoggerFactory _logger;
        private readonly IUserRepository _userRepository;

        public DocumentSentForApprovalDomainEventHandler(
                                ILoggerFactory logger, 
                                IUserRepository userRepository
                            )
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _userRepository = _userRepository ?? throw new ArgumentNullException(nameof(userRepository));
        }

        public async Task Handle(DocumentSentForApprovalDomainEvent documentSentForApprovalDomainEvent, CancellationToken cancellationToken)
        {
            var user = await _userRepository.GetAsync(documentSentForApprovalDomainEvent.SentById);

            _logger.CreateLogger<DocumentSentForApprovalDomainEventHandler>()
                   .LogTrace("Document with Id: {0} has been successfully sent for approval by user {1} with Id: {2}",
                              documentSentForApprovalDomainEvent.DocId, user.GetFIO(), user.Id);
        }
    }
}
