﻿using Bukhatos.Domain.Entities.UserAggregate;
using Bukhatos.Domain.Events.Document;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Bukhatos.Application.DomainEventHandlers
{
    public class DocumentCreatedDomainEventHandler
                                : INotificationHandler<DocumentCreatedDomainEvent>
    {
        private readonly ILoggerFactory _logger;
        private readonly IUserRepository _userRepository;

        public DocumentCreatedDomainEventHandler(
                                ILoggerFactory logger,
                                IUserRepository userRepository
                            )
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _userRepository = _userRepository ?? throw new ArgumentNullException(nameof(userRepository));
        }

        public async Task Handle(DocumentCreatedDomainEvent documentCreatedDomainEvent, CancellationToken cancellationToken)
        {
            var user = await _userRepository.GetAsync(documentCreatedDomainEvent.CreatedById);

            _logger.CreateLogger<DocumentSentForApprovalDomainEventHandler>()
                   .LogTrace("Document with Id: {0} has been successfully created by user {1} with Id: {2}",
                              documentCreatedDomainEvent.DocId, user.GetFIO(), user.Id);
        }
    }
}
