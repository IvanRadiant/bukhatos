﻿using Bukhatos.Domain.Entities.UserAggregate;
using Bukhatos.Domain.Events.Document;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Bukhatos.Application.DomainEventHandlers
{
    public class DocumentApprovedDomainEventHandler
                                : INotificationHandler<DocumentApprovedDomainEvent>
    {
        private readonly ILoggerFactory _logger;
        private readonly IUserRepository _userRepository;

        public DocumentApprovedDomainEventHandler(
                                ILoggerFactory logger,
                                IUserRepository userRepository
                            )
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _userRepository = _userRepository ?? throw new ArgumentNullException(nameof(userRepository));
        }

        public async Task Handle(DocumentApprovedDomainEvent documentApprovedDomainEvent, CancellationToken cancellationToken)
        {
            var user = await _userRepository.GetAsync(documentApprovedDomainEvent.CreatedById);

            _logger.CreateLogger<DocumentSentForApprovalDomainEventHandler>()
                   .LogTrace("Document with Id: {0} has been successfully approved",
                              documentApprovedDomainEvent.DocId, user.GetFIO());

            //TODO: notify the creator of the document
        }
    }
}
