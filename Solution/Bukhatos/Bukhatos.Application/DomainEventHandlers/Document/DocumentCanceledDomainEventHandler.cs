﻿using Bukhatos.Domain.Entities.UserAggregate;
using Bukhatos.Domain.Events.Document;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Bukhatos.Application.DomainEventHandlers
{
    public class DocumentCanceledDomainEventHandler
                                 : INotificationHandler<DocumentCanceledDomainEvent>
    {
        private readonly ILoggerFactory _logger;
        private readonly IUserRepository _userRepository;

        public DocumentCanceledDomainEventHandler(
                                ILoggerFactory logger,
                                IUserRepository userRepository
                            )
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _userRepository = _userRepository ?? throw new ArgumentNullException(nameof(userRepository));
        }

        public async Task Handle(DocumentCanceledDomainEvent documentCanceledDomainEvent, CancellationToken cancellationToken)
        {
            var user = await _userRepository.GetAsync(documentCanceledDomainEvent.CanceledById);

            _logger.CreateLogger<DocumentSentForApprovalDomainEventHandler>()
                   .LogTrace("Document with Id: {0} has been canceled by user {1} with Id: {2}",
                              documentCanceledDomainEvent.DocId, user.GetFIO(), user.Id);
        }
    }
}
