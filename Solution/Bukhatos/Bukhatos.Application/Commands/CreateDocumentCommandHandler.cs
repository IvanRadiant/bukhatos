﻿using System.Threading;
using System.Threading.Tasks;
using Bukhatos.Domain.Entities.DocumentAggregate;
using Bukhatos.Domain.Entities.DocumentTypeAggregate;
using MediatR;

namespace Bukhatos.Application.Commands
{
    public class CreateDocumentCommandHandler : IRequestHandler<CreateDocumentCommand, bool>
    {
        private readonly IDocumentRepository _documentRepository;
        private readonly IDocumentTypeRepository _documentTypeRepository;

        public CreateDocumentCommandHandler(IDocumentRepository documentRepository, IDocumentTypeRepository documentTypeRepository)
        {
            documentRepository = _documentRepository;
            _documentTypeRepository = documentTypeRepository;
        }

        public async Task<bool> Handle(CreateDocumentCommand createDocumentCommand, CancellationToken cancellationToken)
        {
            var docType = await _documentTypeRepository.GetAsync(createDocumentCommand.DocTypeId);
            var doc = docType.CreateNewDocument(createDocumentCommand.UserId);

            _documentRepository.Add(doc);

            return await _documentRepository.UnitOfWork.SaveEntitiesAsync();
        }
    }
}
