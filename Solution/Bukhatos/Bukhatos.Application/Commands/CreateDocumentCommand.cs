﻿using MediatR;

namespace Bukhatos.Application.Commands
{
    public class CreateDocumentCommand : IRequest<bool>
    {
        public int UserId { get; set; }
        public int DocTypeId { get; set; }
        
        //GraphQL
        public CreateDocumentCommand() { }

        public CreateDocumentCommand(int userId, int docTypeId)
        {
            this.UserId = userId;
            this.DocTypeId = docTypeId;
        }
    }
}
